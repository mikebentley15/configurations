;=@echo off
;=setlocal ENABLEEXTENSIONS

;=rem ::
;=rem :: Author:    Michael Bentley
;=rem :: Created:   08 November 2016
;=rem :: Modified:  09 November 2016
;=rem ::
;=rem :: This is both a batch script and a doskey macro file.  It uses the hack
;=rem :: that in bash ;= is simply ignored whereas the doskey program thinks it
;=rem :: is the name of a macro.  To avoid unintended consequences, we reset the
;=rem :: ; macro at the end of the file.
;=rem ::
;=rem :: I did not come up with this trick, I found it on someone's macro file
;=rem :: online.  But I lost that, and remembered the trick.  So I don't know
;=rem :: where it came from.
;=rem ::

;=rem :: These are bash commands that are executed.

;=rem :: Parse arguments
;=if /I "%~1"=="/?" ( goto PRINT_USAGE )
;=if /I "%~1"=="/install" ( goto INSTALL_SCRIPT )
;=goto RUN_DOSKEY



;=rem
;=rem :: Windows command aliases
;=rem

cat=type $*
clear=cls $*
cp=copy $*
h=doskey /history
ls=dir $*
mv=move $*
rm=del $*
which=where $*


;=rem
;=rem :: GE Utilities
;=rem

f5="C:\Program Files (x86)\F5 VPN\f5fpclientW.exe" $*
jabber="C:\Program Files (x86)\Cisco Systems\Cisco Jabber\CiscoJabber.exe" $*
rsa="C:\Program Files (x86)\RSA SecurID Software Token\SecurID.exe" $*


;=rem
;=rem :: From the previous file
;=rem

7z="C:\Program Files\7-Zip\7z.exe" $*
7zfm="C:\Program Files\7-Zip\7zFM.exe" $*
chrome="C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" $*
firefox="C:\Program Files (x86)\Mozilla Firefox\firefox.exe" $*
gimp="C:\Program Files\GIMP 2\bin\gimp-2.8.exe" $*
java="C:\Program Files (x86)\Java\jre1.6.0_22\bin\java.exe" $*
npp="C:\Program Files (x86)\Notepad++\notepad++.exe" $*
synergy="C:\Program Files\Synergy\synergy.exe" $*
synergys="C:\Program Files\Synergy\synergys.exe" $*
synergyc="C:\Program Files\Synergy\synergyc.exe" $*
vlc="C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" $*
xlaunch="C:\Program Files\Xming\XLaunch.exe" $*
xming="C:\Program Files (x86)\Xming\Xming.exe" $*
xmingsetup="C:\Program Files (x86)\Xming\Xming.exe" :0 -clipboard -multiwindow $*
inkscape="C:\Program Files\Inkscape\inkscape.exe" $*
freecad="C:\Program Files\FreeCAD 0.16\bin\FreeCAD.exe" $*

nppalias="C:\Program Files (x86)\Notepad++\notepad++.exe" C:\Users\212072022\bin\alias.bat

;=rem
;=rem :: PuTTY
;=rem

ftp="C:\Program Files\PuTTY\psftp.exe" $*
pageant="C:\Program Files (x86)\PuTTY\pageant.exe" $*
plink="C:\Program Files (x86)\PuTTY\plink.exe" $*
pscp="C:\Program Files (x86)\PuTTY\pscp.exe" $*
psftp="C:\Program Files (x86)\PuTTY\psftp.exe" $*
putty="C:\Program Files (x86)\PuTTY\putty.exe" $*
puttygen="C:\Program Files (x86)\PuTTY\puttygen.exe" $*
scp="C:\Program Files (x86)\PuTTY\pscp.exe" $*
ssh="C:\Program Files (x86)\PuTTY\putty.exe" $*


;=rem
;=rem :: MinGW
;=rem

;=rem :: compiling and build tools with MinGW

g++=C:\MinGW\bin\g++.exe $*
gcc=C:\MinGW\bin\gcc.exe $*
gcov=C:\MinGW\bin\gcov.exe $*
gdb=C:\MinGW\bin\gdb.exe $*
gprof=C:\MinGW\bin\gprof.exe $*
ld=C:\MinGW\bin\ld.exe $*
nm=C:\MinGW\bin\nm.exe $*
objcopy=C:\MinGW\bin\objcopy.exe $*
objdump=C:\MinGW\bin\objdump.exe $*
strip=C:\MinGW\bin\strip.exe $*

;=rem :: switch line endings

dos2unix=C:\MinGW\bin\dos2unix.exe $*
mac2unix=C:\MinGW\bin\mac2unix.exe $*
unix2dos=C:\MinGW\bin\unix2dos.exe $*
unix2mac=C:\MinGW\bin\unix2mac.exe $*

;=rem :: other utilities

strings=C:\MinGW\bin\strings.exe $*
mingwshell=C:\MinGW\msys\1.0\msys.bat $*
shell=C:\MinGW\msys\1.0\msys.bat $*
bunzip2=C:\MinGW\bin\bunzip2.exe $*
bzip2=C:\MinGW\bin\bunzip2.exe $*


;=rem
;=rem :: Msys utilities within MinGW
;=rem

awk=C:\MinGW\msys\1.0\bin\awk.exe $*
basename=C:\MinGW\msys\1.0\bin\basename.exe $*
bash=C:\MinGW\msys\1.0\bin\bash.exe $*
cpio=C:\MinGW\msys\1.0\bin\bsdcpio.exe $*
comm=C:\MinGW\msys\1.0\bin\comm.exe $*
cut=C:\MinGW\msys\1.0\bin\cut.exe $*
diff=C:\MinGW\msys\1.0\bin\diff.exe $*
du=C:\MinGW\msys\1.0\bin\du.exe $*
grep=C:\MinGW\msys\1.0\bin\egrep.exe $*
egrep=C:\MinGW\msys\1.0\bin\egrep.exe $*
file=C:\MinGW\msys\1.0\bin\file.exe $*
find=C:\MinGW\msys\1.0\bin\find.exe $*
gzip=C:\MinGW\msys\1.0\bin\gzip.exe $*
gunzip=C:\MinGW\msys\1.0\bin\gzip.exe --decompress $*
head=C:\MinGW\msys\1.0\bin\head.exe $*
info=C:\MinGW\msys\1.0\bin\info.exe $*
join=C:\MinGW\msys\1.0\bin\join.exe $*
kill=C:\MinGW\msys\1.0\bin\kill.exe $*
less=C:\MinGW\msys\1.0\bin\less.exe $*
make=C:\MinGW\msys\1.0\bin\make.exe $*
md5sum=C:\MinGW\msys\1.0\bin\md5sum.exe $*
paste=C:\MinGW\msys\1.0\bin\paste.exe $*
patch=C:\MinGW\msys\1.0\bin\patch.exe $*
perl=C:\MinGW\msys\1.0\bin\perl.exe $*
printf=C:\MinGW\msys\1.0\bin\printf.exe $*
ps=C:\MinGW\msys\1.0\bin\ps.exe $*
pwd=C:\MinGW\msys\1.0\bin\pwd.exe $*
readlink=C:\MinGW\msys\1.0\bin\readlink.exe $*
rsync=C:\MinGW\msys\1.0\bin\rsync.exe $*
sed=C:\MinGW\msys\1.0\bin\sed.exe $*
seq=C:\MinGW\msys\1.0\bin\seq.exe $*
sha1sum=C:\MinGW\msys\1.0\bin\sha1sum.exe $*
sleep=C:\MinGW\msys\1.0\bin\sleep.exe $*
split=C:\MinGW\msys\1.0\bin\split.exe $*
ssh-keygen=C:\MinGW\msys\1.0\bin\ssh-keygen.exe $*
stat=C:\MinGW\msys\1.0\bin\stat.exe $*
tail=C:\MinGW\msys\1.0\bin\tail.exe $*
tar=C:\MinGW\msys\1.0\bin\tar.exe $*
tee=C:\MinGW\msys\1.0\bin\tee.exe $*
telnet=C:\MinGW\msys\1.0\bin\telnet.exe $*
test=C:\MinGW\msys\1.0\bin\test.exe $*
touch=C:\MinGW\msys\1.0\bin\touch.exe $*
touch=C:\MinGW\msys\1.0\bin\touch.exe $*
tr=C:\MinGW\msys\1.0\bin\tr.exe $*
uniq=C:\MinGW\msys\1.0\bin\uniq.exe $*
vim=C:\MinGW\msys\1.0\bin\vim.exe $*
vimdiff=C:\MinGW\msys\1.0\bin\vimdiff.exe $*


;=rem :: These are not installed yet

;=rem -> dia="C:\Program Files\Dia\bin\diaw.exe"
;=rem -> diff="C:\Program Files\TortoiseSVN\bin\TortoiseIDiff.exe" $*
;=rem -> filezilla="C:\Program Files\FileZilla FTP Client\filezilla.exe" $*
;=rem -> gitbash="C:\Program Files\Git\bin\sh.exe" --login -i $*
;=rem -> gitgui="C:\Program Files\Git\bin\wish.exe" "C:\Program Files\Git\libexec\git-core\git-gui"
;=rem -> merge="C:\Program Files\TortoiseSVN\bin\TortoiseMerge.exe"
;=rem -> qtcreator=C:\Qt\qtcreator-2.5.2\bin\qtcreator.exe $*
;=rem -> slicer="C:\Program Files\Slicer 4.1.1\Slicer.exe" $*
;=rem -> svn="C:\Program Files\SlikSvn\bin\svn.exe" $*
;=rem -> svnadmin="C:\Program Files\SlikSvn\bin\svnadmin.exe" $*
;=rem -> svndumpfilter="C:\Program Files\SlikSvn\bin\svndumpfilter.exe" $*
;=rem -> svnlook="C:\Program Files\SlikSvn\bin\svnlook.exe" $*
;=rem -> svnmucc="C:\Program Files\SlikSvn\bin\svnmucc.exe" $*
;=rem -> svnrdump="C:\Program Files\SlikSvn\bin\svnrdump.exe" $*
;=rem -> svnsync="C:\Program Files\SlikSvn\bin\svnsync.exe" $*
;=rem -> svnversion="C:\Program Files\SlikSvn\bin\svnversion.exe" $*
;=rem -> truecrypt="C:\Program Files\TrueCrypt\TrueCrypt.exe" $*
;=rem -> windirstat="C:\Program Files\WinDirStat\windirstat.exe" $*

;=rem :: This prints the usage information for this script
;=:PRINT_USAGE
;=echo.
;=echo. Usage:
;=echo.   %~n0
;=echo.   %~n0 /?
;=echo.   %~n0 /install
;=echo.
;=echo. Description:
;=echo.   This script manages aliases which in windows are called
;=echo.   doskeys.  There are two modes, either to install the
;=echo.   script into the registry, or to add the aliases to the
;=echo.   current environment
;=echo.
;=echo. Arguments:
;=echo.   [none]     If no arguments are present, then this will
;=echo.              apply the aliases to the current cmd shell.
;=echo.   /?         Show this help documentation and exit
;=echo.   /install   Install this script as cmd.exe startup in the
;=echo.              registry.
;=echo.
;=goto END

;=rem :: This little section installs the script into the registry if
;=rem :: it is not  already there.
;=rem :: 
;=rem :: This section that installs this script when it is run was taken from
;=rem :: another source written by Steve Jansen (stevejansen_github@mac.com)
;=rem :: in April of 2012
;=:INSTALL_SCRIPT
;=echo %~n0: installing %~nx0 as the cmd.exe auto-run script
;=reg ADD "HKCU\Software\Microsoft\Command Processor" /v "AutoRun" /t REG_EXPAND_SZ /d "^"%~f0^""
;=goto END

;=rem :: This calls doskey and passes in itself as the macro file.  This is very
;=rem :: convenient to store the run script with the data too.
;=:RUN_DOSKEY
;=doskey /macrofile=%0
;=rem :: update the path as needed
;=rem -> PATH %PATH%;%ProgramFiles%\SysInternals;
;=goto END




;=rem :: This is the end of the bash file.  All of that middle stuff is
;=rem :: skipped by bash
;=rem :: Note: Need to reset the ; to empty at the very end

;=:END
;=@echo on
;=@exit /B 0

;=

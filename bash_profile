# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

if [ -f "$HOME/.env_vars" ]; then
  source "$HOME/.env_vars";
fi
if [ -f "$HOME/.extend.profile" ]; then
  source "$HOME/.extend.profile";
fi
if [ -d $HOME/.config/bentley/profile.d ]; then
  for file in $HOME/.config/bentley/profile.d/*.sh; do
    source $file
  done
fi
if [ -d $HOME/.config/bentley/bash_profile.d ]; then
  for file in $HOME/.config/bentley/profile.d/*.sh; do
    source $file
  done
fi

if [ "$XDG_CURRENT_DESKTOP" == "KDE" ]; then
  export QT_QPA_PLATFORMTHEME="qt5ct"
fi

# sources the first existing file, with all non-existing is not an error
# if running bash
if [ -n "$BASH_VERSION" ] && [ -f "$HOME/.bashrc" ]; then
  source $HOME/.bashrc 2>/dev/null || true
fi

# vim modelines
# vim: set ft=sh:

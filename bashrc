# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# vim modelines
# vim: set ft=sh:

## Michael's specifications

# Set up PATHs and evn variables for using QT
if [ -d /usr/lib/qt4 ]; then
  QTDIR=$HOME/QtSDK/Desktop/Qt/4.8.0/gcc
  PATH=$PATH:$QTDIR/bin:$HOME/bin/linux:$HOME/bin
fi

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

shopt -s expand_aliases

# Enable history appending instead of overwriting
shopt -s histappend

complete -cf sudo

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
  debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
  xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
  if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
  else
    color_prompt=
  fi
fi

if [ "$color_prompt" = yes ]; then
  PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
  PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
  xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
  *)
    ;;
esac

# Make it so that <CTRL>-S doesn't send the stop command
stty stop ''

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  # first try /etc, then /usr/share/bash-completion
  if   [ -e "/etc/bash_completion" ]; then source /etc/bash_completion
  elif [ -e "/usr/share/bash-completion/bash_completion" ]; then
    source /usr/share/bash-completion/bash_completion
  fi
fi
# enable profile scripts even if this is not a login shell
#[ -e "/etc/profile" ] && source

# my custom configurations
export BENTLEY_CONFIG="$HOME/.config/bentley"
mkdir -p ${BENTLEY_CONFIG}

# my bashrc.d/
for file in ${BENTLEY_CONFIG}/bashrc.d/*sh; do
  source "$file"
done

source "~/.bash_aliases"    2>/dev/null || true
source "~/.bash_completion" 2>/dev/null || true
source "~/.env_vars"        2>/dev/null || true
source "~/.extend.bashrc"   2>/dev/null || true


#export SPACK_ROOT="$HOME/git/spack"
#source "${SPACK_ROOT}/share/space/setup-env.sh" 2>/dev/null || true

# to add more things to be sourced, simply add files or create symbolic links
# in ${BENTLEY_CONFIG}/bashrc.d

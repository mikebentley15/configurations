#!/usr/bin/env python
'''
For remapping the Wacom touchscreen to the laptop screen when connecting or
disconnecting an external monitor.
'''

from __future__ import print_function

import re
import subprocess as subp
import sys

def get_touchscreen_id():
    'Return the id of the touchscreen input device'
    output = subp.check_output(['xinput', '--list'])
    osplit = output.decode('utf-8').splitlines()
    matcher = re.compile(r'Finger.*\sid=(\d+)\s')
    for line in osplit:
        m = matcher.search(line)
        if m:
            return m.group(1)

def get_laptop_screen_id():
    'Return the name of the laptop screen'
    output = subp.check_output(['xrandr', '--listactivemonitors'])
    osplit = output.decode('utf-8').splitlines()
    rejectors = [
        lambda x: not x.startswith(' '),
        lambda x: 'HDMI' in x,
        ]
    for line in osplit:
        if not any(rej(line) for rej in rejectors):
            return line.split()[1][1:]

def map_to_output(input_id, output_id):
    'Maps X11 input between input and output devices'
    if not input_id or not output_id:
        raise ValueError('input and output ids are both needed, cannot be None')
    command = ['xinput', '--map-to-output', input_id, output_id]
    print(' '.join(command))
    subp.check_call(command)

def main(arguments):
    'main logic here'
    if arguments:
        print('This program does not take arguments', file=sys.stderr)
        return 1
    map_to_output(get_touchscreen_id(), get_laptop_screen_id())
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

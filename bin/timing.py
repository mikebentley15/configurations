#!/usr/bin/env python3
'Times a command and outputs to a file'

import argparse
import csv
import subprocess as subp
import sys
from functools import partial
from timeit import Timer

def parse_args(arguments):
    'parse arguments'
    parser = argparse.ArgumentParser(
        description='''
            Times a given command.  The command to be timed is the remaining
            arguments after parsing.
            ''')
    parser.add_argument('-n', '--repeats', default=10, type=int,
                        help='number of times to run')
    parser.add_argument('-o', '--output', default='timing.csv',
                        help='where to store timing results')
    parser.add_argument('--setup', help='setup for the timing command')
    parser.add_argument('-q', '--quiet', action='store_true',
                        help='Suppress output from setup and command')

    args, remaining = parser.parse_known_args(arguments)
    args.command = remaining
    if args.setup: args.setup = args.setup.split()
    return args

def call(command, quiet=True):
    'run the command'
    if command is not None:
        kwargs = {}
        if quiet:
            kwargs['stdout'] = subp.DEVNULL
            kwargs['stderr'] = subp.DEVNULL
        subp.check_call(command, **kwargs)

def main(arguments):
    'main logic'
    args = parse_args(arguments)
    #print(args)
    global_vars = {
        'setup': args.setup,
        'command': args.command,
        'call': partial(call, quiet=args.quiet)
        }
    timer = Timer(
        stmt='call(command)',
        setup='call(setup)',
        globals=global_vars)
    times = timer.repeat(args.repeats, number=1)
    with open(args.output, 'w') as outfile:
        writer = csv.writer(outfile)
        writer.writerow(['#', 'time (sec)'])
        for i, time in enumerate(times):
            writer.writerow([i+1, time])
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

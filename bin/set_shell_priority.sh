#!/bin/bash

echo Make everything this shell calls be of utmost priority
echo useful when wanting very good performance measurements
echo
echo You must source this file into your shell, not call it as a script.
echo if you call it as a script, it will elevate the priority of the created
echo subshell, only to exit - defeating the purpose.
echo
echo Warning: anything called by this process will be higher priority than pretty
echo   much all other processes on the system - use with care

sudo renice -n -20 -p $$

# add to the PS1 variable to indicate a high priority shell
ERS="\[\033[0m\]"    # reset
EHC="\[\033[1m\]"    # hicolor
EINV="\[\033[7m\]"   # inverse background and foreground
EFRED="\[\033[31m\]" # foreground red
EFYEL="\[\033[33m\]" # foreground yellow

export PS1="${EHC}${EFYEL}(${EINV}${EFRED}PRIORITY${ERS}${EHC}${EFYEL})${ERS}$PS1"

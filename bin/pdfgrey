#!/usr/bin/env python

import argparse
import sys
import subprocess as subp
import tempfile
from pathlib import Path


def pdfgrey(infile, outfile=None):
    infile = Path(infile)
    if outfile is None:
        outfile = infile.with_name(f'{infile.stem}-grey.pdf')
    cmd = [
        'gs',
        f'-sOutputFile={outfile}',
        '-sDEVICE=pdfwrite',
        '-dCompatibilityLevel=1.4',
        '-sColorConversionStrategy=Gray',
        '-dProcessColorModel=/DeviceGray',
        '-dPDFSETTINGS=/screen',
        #'-dEmbedAllFonts=true',
        '-dSubsetFonts=true',
        '-dNOPAUSE',
        '-dBATCH',
        str(infile),
    ]
    result = subp.run(cmd)
    return result.returncode

def populate_parser(parser=None):
    if parser is None:
        parser = argparse.ArgumentParser()
    parser.description = 'Convert a pdf to grayscale.'
    parser.add_argument('infile', type=Path)
    parser.add_argument('outfile', type=Path, nargs='?', default=None,
                        help='output file.  Defaults to <input-wo-ext>-grey.pdf')
    return parser

def main(arguments):
    parser = populate_parser()
    args = parser.parse_args(arguments)
    return pdfgrey(args.infile, args.outfile)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

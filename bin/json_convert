#!/usr/bin/env python3
'Converts json formats'

import argparse
import json
import signal
import sys
from contextlib import ExitStack
from pathlib import Path

import msgpack

AVAILABLE_FORMATS = ('json', 'msgpack')

LOADERS = { 'json': json.load, 'msgpack': msgpack.load }
DUMPERS = { 'json': json.dump, 'msgpack': msgpack.dump }

def populate_parser(parser=None):
    'Create or populate parser'
    if parser is None:
        parser = argparse.ArgumentParser()
    parser.formatter_class = argparse.ArgumentDefaultsHelpFormatter
    parser.description = 'Pretty print a json file to the console'
    parser.add_argument('infile', nargs='?',
                        help='file to convert (default is stdin)')
    parser.add_argument('outfile', nargs='?',
                        help='file to write (default is stdout)')
    parser.add_argument(
        '-i', '--input-format', choices=AVAILABLE_FORMATS, default=None,
        help='Format for input.  Typically inferred from file extension.',
    )
    parser.add_argument(
        '-o', '--output-format', choices=AVAILABLE_FORMATS, default=None,
        help='Format for output.  Typically inferred from file extension.',
    )
    parser.add_argument('--json-indent', type=int, default=4, help='indent for json output format')
    parser.add_argument(
        '-c', '--condensed', dest='json_indent', action='store_const', const=None,
        help='Instead of pretty-printing, remove unnecessary whitespace.'
    )
    parser.add_argument('-s', '--sort-keys', action='store_true',
                        help='sort by dict key')
    return parser

def run_on_input(func, infname=None):
    'Run a function on opened input.  If None, then operate on sys.stdin'
    with ExitStack() as cm:
        if infname:
            fin = cm.enter_context(open(infname, 'r', encoding='utf-8'))
        else:
            fin = sys.stdin
        return func(fin)

def run_on_output(func, outfname=None):
    'Run a function on opened output.  If None, then operate on sys.stdout'
    with ExitStack() as cm:
        if outfname:
            fout = cm.enter_context(open(outfname, 'w', encoding='utf-8'))
        else:
            fout = sys.stdout
        return func(fout)

def json_convert(
        infname=None,
        outfname=None,
        in_format=None,
        out_format=None,
        indent=4,
        sort_keys=False
    ):
    'Convert between json file formats'

    if in_format is None and infname is None:
        raise TypeError('Must specify in_format or infname')
    if out_format is None and outfname is None:
        raise TypeError('Must specify out_format or outfname')

    if in_format is None:
        in_format = Path(infname).suffix[1:]
    if out_format is None:
        out_format = Path(outfname).suffix[1:]

    load = LOADERS[in_format]
    dump = DUMPERS[out_format]

    obj = run_on_input(load, infname=infname)

    def json_dump(fout):
        dump(obj, fout, indent=indent, sort_keys=sort_keys)
        if indent is not None:
            fout.write('\n')

    dump_wrap = json_wrap if out_format == 'json' else lambda fout: dump(obj, fout)
    run_on_output(dump_wrap, outfname=outfname)


def main(arguments):
    'Main logic including command-line parsing'
    parser = populate_parser()
    args = parser.parse_args(arguments)

    # ignore SIGPIPE errors, for example: json_convert tmp.json | head -n 10
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)

    json_convert(
        infname=args.infile,
        outfname=args.outfile,
        indent=args.json_indent,
        sort_keys=args.sort_keys,
    )

if __name__ == '__main__':
    main(sys.argv[1:])

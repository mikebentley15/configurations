#!/usr/bin/env python3

from __future__ import annotations

import argparse
import dataclasses
import subprocess as subp
import sys
import itertools as it
from pathlib import Path
from typing import Callable, Any

import pandas as pd

@dataclasses.dataclass
class GitBreakdownEntry:
    """A single entry from those returned by gdiff_breakdown()."""
    category: str      # category name
    lines_added: int   # lines added reported by the diff
    lines_removed: int # lines removed reported by the diff
    files: list[str]   # list of files matching this category

def is_test(filename: str) -> bool:
    """Return True if the file is considered in a test directory."""
    test_directory_names = set(["test", "TestHelpers"])
    return any(part in test_directory_names for part in Path(filename).parts)

def is_header(filename: str) -> bool:
    """Return True if the file is considered a C or C++ header file."""
    header_suffixes = set([".h", ".ipp", ".hpp", ".hxx", ".hh", ".tpp", ".txx", ".cuh"])
    return Path(filename).suffix in header_suffixes

def is_source(filename: str) -> bool:
    """Return True if the file is considered a C, C++, or Python source file."""
    source_suffixes = set([".c", ".py", ".cpp", ".cxx", ".cc"])
    return Path(filename).suffix in source_suffixes

def is_bazel(filename: str) -> bool:
    """Return True if the file is considered a bazel file."""
    bazel_suffixes = set([".bz"])
    path = Path(filename)
    return path.suffix in bazel_suffixes or path.name == "BUILD"

def is_markdown(filename: str) -> bool:
    """Return True if the file is considered a markdown file."""
    markdown_suffixes = set([".md"])
    return Path(filename).suffix in markdown_suffixes

def is_other(filename: str) -> bool:
    """Return True if the file is considered not any other subcategory."""
    return (not is_header(filename) and
            not is_source(filename) and
            not is_bazel(filename) and
            not is_markdown(filename))

# Mapping of name -> (is_in_function, description)
CATEGORIES: dict[str, tuple[Callable[[str], bool], str]] = {
    "all": ((lambda x: True), "All files."),
    "test": (is_test, "All files in a test directory or in a TestHelpers directory."),
    "src": ((lambda x: not is_test(x)), "All files not in a test directory."),
}
SUBCATEGORIES = {
    "header": (is_header, "C or C++ header files."),
    "source": (is_source, "C, C++, or Python source files."),
    "bazel": (is_bazel, "Bazel files."),
    "markdown": (is_markdown, "Markdown files."),
    "other": (is_other, "All files not in another subcategory."),
}

def gdiff_output_to_dataframe(lines: list[str]) -> pd.DataFrame:
    """Convert console output from 'git diff --numstat' to a pandas DataFrame.

    Generated columns will be 'lines_added', 'lines_removed', 'filename'

    >>> gdiff_output_to_dataframe(["1 2 f1", "3 4 f2"])
       lines_added  lines_removed  filename
    0            1              2  f1
    1            3              4  f2
    """
    lines = [line.strip() for line in lines]
    splitlines = [line.split() for line in lines if line]
    assert all(len(splitline) >= 3 for splitline in splitlines)
    data = [{"lines_added": int(lines_added), "lines_removed": int(lines_removed), "filename": filename}
            for lines_added, lines_removed, filename, *_ in splitlines
            if lines_added != "-" and lines_removed != "-"]
    return pd.DataFrame(data=data)

def category_map_to_dataframe(category_map: dict[str, tuple[Any, str]]) -> pd.DataFrame:
    """Convert a category map to a dataframe for informational purposes."""
    return pd.DataFrame(data=[{"name": name, "description": desc} for (name, (_, desc)) in category_map.items()])

def gen_category_description_dataframe() -> pd.DataFrame:
    """Create the category mapping with descriptions as a DataFrame."""
    df_cat = category_map_to_dataframe(CATEGORIES)
    df_cat["type"] = "Category"
    df_subcat = category_map_to_dataframe(SUBCATEGORIES)
    df_subcat["type"] = "Subcategory"
    df = pd.concat([df_cat, df_subcat], ignore_index=True)
    return df[["type", "name", "description"]]

def add_category_columns(df: pd.DataFrame, inplace: bool = False) -> pd.DataFrame:
    """Add columns 'is_<name>' for name in CATEGORIES or SUBCATEGORIES using the 'filename' column."""
    if not inplace:
        df = df.copy()
    for (name, (is_in_func, _)) in it.chain(CATEGORIES.items(), SUBCATEGORIES.items()):
        df[f"is_{name}"] = [is_in_func(filename) for filename in df["filename"]]
    return df

def gdiff_breakdown(git_diff_args: list[str]) -> list[GitBreakdownEntry]:
    """Provides a breakdown of how the change is split up."""
    proc = subp.run(["git", "diff", "--numstat", *git_diff_args], check=True, text=True, capture_output=True)
    df = add_category_columns(gdiff_output_to_dataframe(proc.stdout.splitlines()), inplace=True)
    breakdown = []
    for category in CATEGORIES:
        cat_df = df.loc[df[f"is_{category}"]]
        breakdown.append(GitBreakdownEntry(
            category=category,
            lines_added = cat_df["lines_added"].sum(),
            lines_removed = cat_df["lines_removed"].sum(),
            files = list(cat_df["filename"]),
        ))
        for subcategory in SUBCATEGORIES:
            cat_and_sub_df = cat_df.loc[cat_df[f"is_{subcategory}"]]
            breakdown.append(GitBreakdownEntry(
                category=f"{category}/{subcategory}",
                lines_added = cat_and_sub_df["lines_added"].sum(),
                lines_removed = cat_and_sub_df["lines_removed"].sum(),
                files = list(cat_and_sub_df["filename"]),
            ))
    return breakdown

def populate_parser(parser: argparse.ArgumentParser | None = None) -> argparse.ArgumentParser:
    """Populate the CLI parser and return it.  Creates one if None is passed in."""
    if parser is None:
        parser = argparse.ArgumentParser()
    parser.add_argument("--help-categories", action="store_true", help="Print information about each category")
    parser.add_argument("--format", default="markdown", choices=("markdown", "csv"), help="Output format")
    parser.add_argument("-o", "--output", type=Path, default=None, help="Output path.  Default to stdout.")
    parser.add_argument("git_diff_args", nargs="*", help="Arguments passed to git diff")
    return parser

def main(arguments: list[str]) -> int:
    """Main logic here."""
    parser = populate_parser()
    args = parser.parse_args(arguments)

    if args.help_categories:
        df_to_print = gen_category_description_dataframe()
    else:
        breakdown = gdiff_breakdown(args.git_diff_args)
        df = pd.DataFrame(data=[dataclasses.asdict(x) for x in breakdown])
        df_to_print = df.drop(columns="files")
        df_to_print = df_to_print.loc[(df["lines_added"] > 0) | (df["lines_removed"] > 0)]

    if args.output is not None:
        print(f"Writing to {args.output} with format {args.format}.")

    if args.format == "markdown":
        content = df_to_print.to_markdown(index=False)
    elif args.format == "csv":
        content = df_to_print.to_csv(index=False)
    else:
        print(f"Error: unsupported output format: {args.format}", file=sys.stderr)
        return 1

    if args.output is None:
        print(content)
    else:
        with args.output.open("w") as fout:
            print(content, file=fout)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))

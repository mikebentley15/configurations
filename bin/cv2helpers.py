from contextlib import contextmanager
import logging

import cv2

def as_VideoCapture(fname_or_cap):
    'If not a cv2.VideoCapture, pass to the constructor'
    try:
        cap = cv2.VideoCapture(fname_or_cap)
    except TypeError:
        cap = fname_or_cap
    return cap

def video_info(fname_or_cap):
    'Return a dictionary of info about the video file'
    cap = as_VideoCapture(fname_or_cap)
    info = {}
    info['num_frames'] = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    info['height'] = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    info['width'] = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    info['fps'] = float(cap.get(cv2.CAP_PROP_FPS))
    return info

class LazyFrame:
    def __init__(self, cap, frameno):
        self.cap = cap
        self.frame = None
        self.frameno = frameno
        self._called = False

    def __call__(self):
        'Cache result of retrieve()'
        if not self._called:
            success, self.frame = self.cap.retrieve()
            self._called = True
            if not success:
                logging.warning()
                raise IOError('Could not retrieve frame {self.frameno}')
        return self.frame

def get_lazy_frames(fname_or_cap):
    '''Return a generator of LazyFrame objects.

    Example usage:
        for lazyframe in get_lazy_frames('video.mp4'):
            print(f'reading frame {lazyframe.frameno}')
            try:
                frame = lazyframe()
            except IOError:
                print('Failed to read the frame')
                raise
            # use frame
    '''
    cap = as_VideoCapture(fname_or_cap)
    frameno = -1
    while cap.isOpened():
        success = cap.grab()
        if not success:
            logging.debug(f'Finished reading at frame {frameno}')
            break
        frameno += 1
        yield LazyFrame(cap, frameno)

def get_frames(fname_or_cap):
    'Return a generator of frames'
    for lazyframe in get_lazy_frames(fname_or_cap):
        try:
            frame = lazyframe()
        except IOError:
            logging.warning('Could not retrieve frame {lazyframe.frameno}')
            break
        else:
            yield frame

class Cv2ShowWriter:
    '''
    Wrapper around cv2.VideoWriter that shows too.
    Press Q to quit (raises a KeyboardInterrupt)
    '''

    def __init__(self, writer, delay=1):
        self.writer = writer
        self.delay = delay

    def write(self, frame, *args, **kwargs):
        self.writer.write(frame, *args, **kwargs)
        cv2.imshow('frame', frame)
        # press Q on keyboard to exit
        if cv2.waitKey(self.delay) & 0xFF == ord('q'):
            raise KeyboardInterrupt('User requested a quit')

    def release(self):
        self.writer.release()

@contextmanager
def cv2_context(*to_release):
    try:
        yield to_release # start of with statement
    finally:
        for x in to_release:
            x.release()
        cv2.destroyAllWindows()



;; Set the load path for the emacs directory I created for scripts
(add-to-list 'load-path "~/.config/bentley/emacs_scripts/")

;; Scripts from ~/.config/bentley/emacs_scripts/ to load always
(require 'column-marker)

;; Set the tab width as 2, meaning the size of tabs
(setq default-tab-width 2)
(setq c-basic-offset 2)

;; Have visual line mode enabled by default
(setq global-visual-line-mode 1)

;; Set the auto-indent to happen when you press RET
(add-hook 'c-mode-common-hook '(lambda ()
  (local-set-key (kbd "RET") 'newline-and-indent)))

;; Sets the M-n key to scroll the screen down one line without
;; moving the cursor (except if the cursor would be off of the 
;; screen).  Likewise, sets the M-p key to scroll up one line.
(global-set-key (kbd "M-n") (lambda() (interactive) (scroll-up 1)))
(global-set-key (kbd "M-p") (lambda() (interactive) (scroll-down 1)))

;; When in shell mode, use AnsiTerm which supports colors 
;; instead of showing escape sequences.
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;; Scrolling settings (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(2 ((shift) . 1))) ;; scroll wheel: 2 at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time

;; Turn on CUA which allows for C-x, C-c, and C-p for cut copy
;; and paste respectively.  The C-x and C-c only do cut and copy
;; when something is highlighted.  Also, you can do rectangle
;; mode to edit multiple lines with C-Enter
(cua-mode t)
(setq cua-enable-cua-keys nil)
(setq cua-highlight-region-shift-only t) ;; no transient mark mode
(setq cua-toggle-set-mark nil) ;; original set-mark behavior

;; Define cuda mode
(autoload 'cuda-mode "cuda-mode.el" "Cuda mode." t)

;; Define csharp mode
(autoload 'csharp-mode "csharp-mode" "Major mode for editing C# code." t)
(require 'csharp-mode)


;; Default major modes for file types

;; Have .h files opened in c++-mode
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
;; Have .C files opened in c++-mode
(add-to-list 'auto-mode-alist '("\\.C\\'" . c++-mode))
;; Have .cu files opened in cuda mode
(setq auto-mode-alist (append '(("/*.\.cu$" . cuda-mode)) auto-mode-alist))
;; Have .hu files opened in cuda mode
(setq auto-mode-alist (append '(("/*.\.hu$" . cuda-mode)) auto-mode-alist))
;; Have .tex files opened in LaTeX mode
(add-to-list 'auto-mode-alist '("\\.tex\\'" . latex-mode))


;; Default minor modes for file types
;; - displays a mark on the 80th character of each line
(add-hook 'c++-mode-hook (lambda () (interactive) (column-marker-1 80)))
(add-hook 'c-mode-hook   (lambda () (interactive) (column-marker-1 80)))


;;;; ---=== Load sripts in the .emacs_scripts directory ===--- ;;;;

;; C++ customizations
(add-hook 'c++-mode-hook (lambda () (hs-minor-mode 1)))
;(add-hook 'c++-mode-hook 'my-cpp-customizations)
;(defun my-cpp-customizations ()
;	(require 'cpp-custom nil t))
(c-set-offset 'substatement-open 0)


;; Outline-minor-mode customizations
;(add-hook 'outline-minor-mode-hook 'my-outline-minor-mode-customizations)
;(defun my-outline-minor-mode-customizations ()
;  (require 'outline-mode-easy-bindings nil t))


;; HideShow minor mode customizations
;; TODO: Get this file to work for hideshow.  It is a copy of the 
;;       outline-mode-easy-bindings file.
(add-hook 'hs-minor-mode-hook 'my-hs-minor-mode-customizations)
(defun my-hs-minor-mode-customizations ()
  (require 'hs-minor-mode-easy-bindings nil t))


;; Soft line wrapping and soft indentations to be enabled in 
;;   - LaTeX mode
;;   - text mode
(add-hook 'latex-mode-hook 'my-adaptive-wrap)
(add-hook 'text-mode-hook 'my-adaptive-wrap)
(defun my-adaptive-wrap ()
  (require 'adaptive-wrap nil t))



(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(cua-mode t nil (cua-base))
 '(indent-tabs-mode nil)
 '(show-paren-mode t)
 '(standard-indent 4)
 '(tab-always-indent nil))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "white" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 98 :width normal :foundry "unknown" :family "DejaVu Sans Mono")))))

;; vim modelines
;; vim: set ft=lisp:

local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
local null_ls = require("null-ls")

local b = null_ls.builtins

local opts = {
  sources = {
    -- multiple
    -- Limit prettier to work with only these file types.
    b.formatting.prettier.with({
      filetypes = {
        "html",
        "markdown",
        "css",
        --"json",
        --"yaml",
      },
    }),

    -- lua
    b.formatting.stylua,

    -- cpp
    b.formatting.clang_format,

    -- linter
    b.diagnostics.vale,
  },
  on_attach = function(client, bufnr)
    if client.supports_method("textDocument/formatting") then
      vim.api.nvim_clear_autocmds({
        group = augroup,
        buffer = bufnr,
      })
      vim.api.nvim_create_autocmd("BufWritePre", {
        group = augroup,
        buffer = bufnr,
        callback = function()
          vim.lsp.buf.format({ bufnr = bufnr })
        end,
      })
    end
  end,
}
return opts

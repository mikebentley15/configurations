local M = {}

M.git = {
	enable = true,
}
M.renderer = {
	highlight_git = true,
	icons = {
		show = {
			git = true,
		},
	},
}
M.log = {
	enable = true,
	truncate = true,
	types = {
		all = false,
		config = false,
		copy_paste = false,
		dev = false,
		diagnostics = false,
		git = true,
		profile = true,
		watcher = true,
	},
}
M.filesystem_watchers = {
	enable = true,
	ignore_dirs = {
		--".git",
		".cache",
		"releasenotes",
		"bazel-bin",
		"bazel-devel",
		"bazel-out",
		"bazel-testlogs",
		"external",
		--"vpb",
		-- "tox",
	},
}

return M

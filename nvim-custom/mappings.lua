local M = {}

M.dap = {
	plugin = true,
	n = {
		["<leader>db"] = {
			"<cmd> DapToggleBreakpoint <CR>",
			"Add breakpoint at line",
		},
		["<leader>dr"] = {
			"<cmd> DapContinue <CR>",
			"Start or continue the debugger",
		},
	},
}

local tabufline = function()
	return require("nvchad.tabufline")
end
local moveBufLeft = function()
	tabufline().move_buf(-1)
end
local moveBufRight = function()
	tabufline().move_buf(1)
end
local gotoBufLeft = function()
	tabufline().tabuflinePrev()
end
local gotoBufRight = function()
	tabufline().tabuflineNext()
end

M.tabufline = {
	plugin = true,

	i = {
		["<S-F3>"] = { moveBufLeft, "Move tab left" },
		["<S-F4>"] = { moveBufRight, "Move tab right" },
		["<F3>"] = { gotoBufLeft, "Goto prev buffer" },
		["<F4>"] = { gotoBufRight, "Goto next buffer" },
	},

	n = {
		["<S-F3>"] = { moveBufLeft, "Move tab left" },
		["<S-F4>"] = { moveBufRight, "Move tab right" },
		["<F3>"] = { gotoBufLeft, "Goto prev buffer" },
		["<F4>"] = { gotoBufRight, "Goto next buffer" },
	},
}

M.crates = {
	n = {
		["<leader>rcu"] = {
			function()
				require("crates").upgrade_all_crates()
			end,
			"update crates",
		},
	},
}

return M

local opt = vim.opt

-- Folding
-- opt.foldmethod = "expr"
-- opt.foldexpr = "nvim_treesitter#foldexpr()"
opt.foldmethod = "syntax"

-- Indentation
vim.api.nvim_create_autocmd("FileType", {
	pattern = "cpp,python",
	callback = function()
		opt.shiftwidth = 4
		opt.tabstop = 4
		opt.softtabstop = 4
	end,
})

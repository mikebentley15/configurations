#!/usr/bin/bash

# update existing packages
sudo apt update
sudo apt upgrade

# install lots of things

# general utilities, apps, and tools
sudo apt install \
  aspell \
  aspell-en \
  bash-completion \
  bsdutils \
  cheese \
  coreutils \
  curl \
  default-jdk \
  dia \
  docker.io \
  dropbox \
  dvdauthor \
  elinks \
  elinks-data \
  elinks-doc \
  encfs \
  ffmpeg \
  ffmpeg-doc \
  fonts-dejavu \
  fonts-freefont-otf \
  fonts-freefont-ttf \
  gimp \
  gimp-data-extras \
  gimp-help-en \
  gparted \
  graphviz \
  graphviz-doc \
  htop \
  imagemagick \
  imagemagick-doc \
  inkscape \
  inkscape-tutorials \
  iputils-ping \
  kdenlive \
  libevdev-dev \
  libyaml-cpp-dev \
  lsb-release \
  man-db \
  meshlab \
  mlocate \
  moreutils \
  okular \
  openssh-server \
  p7zip-full \
  pandoc \
  pandoc-citeproc \
  picocom \
  psutils \
  python-is-python3 \
  python3 \
  python3-pip \
  python3-pygments \
  qpdfview \
  screen \
  scribus \
  software-properties-common \
  sshfs \
  subversion \
  subversion-tools \
  sudo \
  terminator \
  tmux \
  tree \
  udiskie \
  vagrant \
  viewnior \
  vim \
  vim-doc \
  vim-gtk3 \
  vim-scripts \
  virtualbox \
  vlc \
  wget \
  x2goclient \

# developer tools
sudo apt install \
  build-essential \
  cgdb \
  clang-12 \
  clang-12-doc \
  clang-12-examples \
  cmake \
  cmake-curses-gui \
  cmake-doc \
  cmake-extras \
  cmake-qt-gui \
  cppcheck \
  cppcheck-gui \
  cppreference-doc-en-html \
  cppreference-doc-en-qch \
  creduce \
  cython-doc \
  cython3 \
  debhelper \
  devhelp \
  devscripts \
  diffutils \
  dochelp \
  doxygen \
  doxygen-doc \
  doxygen-gui \
  doxygen-latex \
  extra-cmake-modules \
  exuberant-ctags \
  gcc-10 \
  gcc-10-doc \
  gdb \
  gdb-doc \
  geany-plugin-ctags \
  gfortran-10 \
  gfortran-10-doc \
  git \
  git-doc \
  git-gui \
  git-man \
  gitk \
  gnupg2 \
  gnuplot-doc \
  google-mock \
  ipython3 \
  jupyter \
  jupyter-client \
  jupyter-console \
  jupyter-nbconvert \
  jupyter-qtconsole \
  latexdiff \
  latexmk \
  lcov \
  libboost-all-dev \
  libboost-doc \
  libgtest-dev \
  libomp-12-dev \
  libomp-12-doc \
  llvm-12-doc \
  make \
  ninja-build \
  octave \
  octave-doc \
  octave-image \
  octave-io \
  octave-linear-algebra \
  octave-miscellaneous \
  octave-missing-functions \
  octave-optiminterp \
  octave-quaternion \
  octave-symbolic \
  openscad \
  openscad-mcad \
  pylint \
  pyqt5-dev \
  pyqt5-dev-tools \
  pyqt5-examples \
  pyqt5chart-dev \
  python-nbconvert-doc \
  python-notebook-doc \
  python-numpy-doc \
  python-pandas-doc \
  python-scipy-doc \
  python-sympy-doc \
  python3-dev \
  python3-matplotlib \
  python3-nose \
  python3-numpy \
  python3-pandas \
  python3-pyqt5 \
  python3-scipy \
  qt5-assistant \
  qt5-default \
  qt5-doc \
  qt5-doc-html \
  qt5-image-formats-plugins \
  qt5-qmake \
  qtbase5-dev \
  qtcreator \
  ruby \
  sqlite3 \
  sqlite3-doc \
  texlive-full \
  valgrind \
  xxgdb \

# dev-env-specific dependencies
sudo apt install \
  arduino \
  arduino-builder \
  arduino-mk \
  binutils-arm-none-eabi \
  dcmtk \
  debianutils \
  dicom3tools \
  gcc-arm-none-eabi \
  hdf5-helpers \
  hdf5-tools \
  insighttoolkit4-examples \
  insighttoolkit4-python3 \
  libeigen3-dev \
  libeigen3-doc \
  libf2c2-dev \
  libfcl-dev \
  libgsl-dev \
  libhdf5-dev \
  libhdf5-doc \
  libinsighttoolkit4-dev \
  libqcustomplot-dev \
  libqcustomplot-doc \
  libqt5serialport5-dev \
  libreadline-dev \
  pybind11-dev \
  pybind11-doc \
  python3-dotenv \
  python3-dotenv-cli \
  python3-pyelftools \
  python3-serial \
  python3-toml \
  qt5serialport-examples \
  readline-doc \

# Note: our dev environment needs OMPL 1.5.  Ubuntu provides OMPL 1.4.2.
# So, instead, we install OMPL using the version available for ROS2 which
# is OMPL 1.5.0.  This is done after installing ROS2 below.

git config --global user.email "mikebentley15@gmail.com"
git config --global user.name "Michael Bentley"

# copy over ssh keys and known hosts from last OS, which is already paired with
# GitHub and Bitbucket
mkdir -p ~/.ssh
cp /mnt/manjaro/home/bentley/.ssh/{id_rsa*,config,known_hosts} ~/.ssh

# get my custom configurations and setup
mkdir git
pushd git
git clone git@bitbucket.org:mikebentley15/configurations
pushd configurations
git submodule update --init
make backup -f link-config.mk
make all -f link-config.mk
popd
popd
source ~/.bashrc # source the new bashrc

# Carmen's Right key is broken, so it and the left one is swapped with the
# ones above them.  The solution below with xmodmap works pretty well, but
# has some issues when the keyboard is reloaded or with Google Chrome.  For
# that reason, a different approach was taken below.
#
# This other approach uses something in my sandbox repo.  Search for
#   ==SWAP KEYS==
# in the comments below.
#
## apply the xmodmap now
#xmodmap ~/.xmodmap
#
## setup xmodmap to load at startup (i.e., at login to my account)
#mkdir -p ~/.config/autostart
#cat > ~/.config/autostart/KeyboardRemap.desktop <<EOF
#[Desktop.Entry[
#Type=Application
#Name=KeyboardRemap
#Exec=xmodmap $HOME/.xmodmap
#StartupNotify=false
#Terminal=false
#EOF
#
# we will use a different approach after we have cloned our sandbox repo
rm ~/.xmodmap


# Add the ROS 2 apt repository
sudo curl \
  -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key \
  -o /usr/share/keyrings/ros-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu focal main" |
  sudo tee /etc/apt/sources.list.d/ros2.list
sudo apt update
sudo apt install ros-foxy-desktop
sudo apt install \
  ros-foxy-rosidl-typesupport-connext-c \
  ros-foxy-rosidl-typesupport-connext-cpp \
  python3-colcon-common-extensions
source /opt/ros/foxy/setup.bash
# Install OMPL in ROS since it's newer than what's in the ubuntu repos
suto apt install ros-foxy-ompl

#
# Setup 3rdparty git libraries
#

mkdir -p ~/3rdparty
pushd ~/3rdparty

# ampy
git clone --origin fork git@github.com:mikebentley15/ampy
pushd ampy
git remote add origin https://github.com/scientifichackers/ampy
git fetch origin
popd
cat > ~/bin/ampy <<EOF
#!/usr/bin/env python3

import os
import sys

ampy_dir = os.path.join(
        os.environ['HOME'],
        '3rdparty',
        'ampy')

sys.path.insert(0, ampy_dir)

from ampy import cli

cli.cli()
EOF
chmod +x ~/bin/ampy

# fcl
git clone --branch 0.5.0 https://github.com/flexible-collision-library/fcl

# Gomagic Touch ROS Drivers
git clone --origin fork --branch hydro-devel git@github.com:Utah-ARMLab/geomagic_touch_ros_drivers
pushd geomagic_touch_ros_drivers
git remote add origin https://github.com/bharatm11/Geomagic_Touch_ROS_Drivers
git fetch origin
popd

# ros2qt
git clone https://github.com/1r0b1n0/libros2qt

# micropython
git clone --origin fork --branch bentley git@github.com:mikebentley15/micropython
pushd micropython
git remote add origin https://github.com/micropython/micropython
git fetch origin
git submodule init lib/axtls
git submodule init lib/berkeley-db-1.xx
git submodule init lib/tinyusb
git submodule update
popd

# micropython-lib
git clone --origin fork --branch bentley git@github.com:mikebentley15/micropython-lib
pushd micropython-lib
git remote add origin https://github.com/micropython/micropython-lib
git fetch origin
popd

# microRos pi pico
git clone https://github.com/micro-ROS/micro_ros_raspberrypi_pico_sdk microros-pico

# ompl
git clone --branch 1.4.2 https://github.com/ompl/omplapp
pushd omplapp
# submodule added somewhere between 1.4.2 and 1.5.0
git clone --branch 1.4.2 https://github.com/ompl/ompl
popd

# pico-examples
git clone https://github.com/raspberrypi/pico-examples

# pico-sdk
git clone https://github.com/raspberrypi/pico-sdk
pushd pico-sdk
git remote add fork git@github.com:mikebentley15/pico-sdk
git fetch fork
git submodule init lib/tinyusb
git submodule update
popd

# pio-emulator
git clone https://github.com/soundpaint/rp2040pio pio-emulator
pushd pio-emulator
make
popd

# qt5
git clone --branch 5.12.8 https://github.com/qt/qt5.git
pushd qt5
./init-repository
popd

# build micropython
pushd micropython/ports/unix
make -j4
popd
ln -s $HOME/3rdparty/micropython/ports/unix/micropython ~/bin/micropython
ln -s micropython ~/bin/upy
mkdir -p micropython/ports/rp2/build
pushd micropython/ports/rp2/build
cmake .. \
  -GNinja \
  -DCMAKE_BUILD_TYPE=Release \
  -DPICO_SDK_PATH_OVERRIDE=$HOME/3rdparty/pico-sdk
ninja -j4
popd
mkdir -p micropython/ports/rp2/build-minsize
pushd micropython/ports/rp2/build-minsize
cmake .. \
  -GNinja \
  -DCMAKE_BUILD_TYPE=MinSizeRel \
  -DPICO_SDK_PATH_OVERRIDE=$HOME/3rdparty/pico-sdk
ninja -j4
popd

# build pico-examples
mkdir -p pico-examples/build
pushd pico-examples/build
cmake .. \
  -GNinja \
  -DCMAKE_BUILD_TYPE=Release \
  -DPICO_SDK_PATH=$HOME/3rdparty/pico-sdk
ninja -j4
popd

popd

#
# finished with 3rdparty
#

#
# my git repos
#

# utahtelerobotics
mkdir -p git/armlab/utahtelerobotics
pushd git/armlab/utahtelerobotics
git clone git@bitbucket.org:utahtelerobotics/roboticsframework
mkdir -p roboticsframework/build
pushd roboticsframework/build
cmake .. \
  -GNinja \
  -DCMAKE_BUILD_TYPE=Release \
  -DROBOTICS_USE_CV=ON \
  -DROBOTICS_USE_EXAMPLES=ON \
  -DROBOTICS_USE_OPTIMIZATION=ON
ninja -j2
popd
popd

# armlab
mkdir -p git/armlab
pushd git/armlab
mkdir -p collaborators
git clone git@github.com:mfrox/tendonrobottest collaborators/tendonrobottest
git clone git@github.com:utah-armlab/needle-steering
mkdir -p needle-steering/build
pushd needle-steering/build
cmake .. \
  -GNinja \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_PREFIX_PATH=$HOME/git/armlab/utahtelerobotics/roboticsframework/build
ninja -j2
popd
git clone git@bitbucket.org:mikebentley15/tendon_experiments
mkdir -p tendon_experiments/build
pushd tendon_experiments/build
cmake .. \
  -GNinja \
  -DCMAKE_BUILD_TYPE=Release
ninja -j2
popd
popd

# armlab papers
mkdir -p git/armlab/papers
pushd git/armlab/papers
git clone git@github.com:utah-armlab/paper-2020-tendon-motion-planning \
  2020-tendon-motion-planning
git clone git@github.com:utah-armlab/paper-2021-iros-needle-tissue-force \
  2021-iros-needle-tissue-force
git clone git@github.com:utah-armlab/thesis-proposal-bentley \
  bentley-thesis-proposal
git clone git@github.com:utah-armlab/huang2021_iros huang-2021-iros
git clone git@github.com:utah-armlab/huang2021_jmrr huang-2021-jmrr
popd

# ll4ma
mkdir -p git/ll4ma
pushd git/ll4ma
git clone git@bitbucket.org:robot-learning/bitstar_experiments
git clone git@bitbucket.org:robot-learning/deb-builder
git clone git@bitbucket.org:robot-learning/ll4ma-opt-sandbox
mkdir -p posters
pushd posters
git clone git@github.com:utah-armlab/poster-bentley-2020-gradvisit \
  2020-02-14-grad-visit
git clone git@github.com:utah-armlab/poster-bentley-2021-gradvisit \
  2021-03-05-grad-visit
popd
popd

# pruners
mkdir -p git/pruners
pushd git/pruners
git clone git@github.com:pruners/archer
git clone git@github.com:pruners/archer-gui
git clone git@github.com:pruners/flit
git clone git@github.com:mikebentley15/lulesh
git clone git@github.com:pruners/meeting-notes
git clone https://github.com/mfem/mfem
git clone git@github.com:pruners/flit_mfem mfem-tests
git clone --recurse-submodules git@github.com:pruners/research-papers papers
git clone git@github.com:pruners/pruners.github.io
git clone git@github.com:pruners/rempi
pushd rempi
git remote add fork git@github.com:mikebentley15/rempi
git fetch fork
popd
git clone --origin fork git@github.com:mikebentley15/spack
pushd spack
git remote add origin https://github.com/spack/spack
git fetch origin
git branch origin-develop
git branch origin-develop -u origin/develop
popd
mkdir -p tutorials
pushd tutorials
git clone git@github.com:PRUNERS/flit_tutorial flit-tutorial
git clone git@github.com:PRUNERS/sc19_rempi_examples sc19-rempi-examples
popd
popd

# other git repos
mkdir -p git
pushd git
git clone git@bitbucket.org:mikebentley15/finance-scripts
ln -s pruners/flit flit
git clone --branch simple git@github.com:mikebentley15/my-c-compiler
git clone git@bitbucket.org:mikebentley15/qhometheater
git clone git@github.com:soarlab/written-quals quals-written
git clone git@github.com:mikebentley15/sandbox
pushd sandbox
git submodule init arduino/3rdparty/AccelStepper
git submodule init arduino/3rdparty/DIO2
git submodule init arduino/3rdparty/MsTimer2
git submodule init arduino/3rdparty/64arduinoAdapter
git submodule init arduino/3rdparty/pico-pi-playing
git submodule init arduino/3rdparty/rpi
git submodule init cmake/tutorials/cmake-template-example
git submodule init python/blender/blender-cli-rendering
git submodule init python/micropython/pi-pico/pico-micropython-examples
git submodule init c/udev/interception/tools
git submodule update
popd
git clone git@bitbucket.org:mikebentley15/tdd-book
popd

#   ==SWAP KEYS==
# Here is where I setup the replacement of the xmodmap solution I had been
# usig for years.  Instead, this uses interception-tools which leverages
# libudev to change input values BEFORE it gets to X11.
#
# This solution will work with X11, Wayland, or anything that reads raw
# events rather than ones given by X11.
mkdir -p git/sandbox/c/udev/interception/build
pushd git/sandbox/c/udev/interception/build
cmake .. -GNinja -DCMAKE_BUILD_TYPE=Release
ninja
sudo ninja install
sudo cp udevmon.service /usr/lib/systemd/system/
sed -i -e 's,/usr/bin/udevmon,/usr/local/bin/udevmon' \
  /usr/lib/systemd/system/udevmon.service
popd
mkdir -p git/sandbox/c/udev/interception/homeswap/build
pushd git/sandbox/c/udev/interception/homeswap/build
cmake .. -GNinja -DCMAKE_BUILD_TYPE=Release
ninja
sudo ninja install carmen-arrowfix
popd
sudo mkdir -p /etc/interception
cat <<EOF | sudo tee /etc/interception/udevmon.yaml
- JOB: intercept -g \$DEVNODE | carmen-arrowfix | uinput -d \$DEVNODE
  DEVICE:
    LINK: /dev/input/by-path/platform-i8042-serio-0-event-kbd
EOF
sudo systemctl daemon-reload
sudo systemctl enable udevmon
sudo systemctl start udevmon
# now the old back and forward buttons should be mapped as left and right
# arrow keys, with the old left and right keys doing nothing


# TODO: google-chrome
# TODO: code # vscode
# TODO: zoom
# TODO: ros1
# TODO: docker containers in /home/docker
# TODO: zotero
# TODO: miniconda
# TODO: copy files from pi:/mnt/archive


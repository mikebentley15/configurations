# Carmen Laptop Setup

Here are the steps I took to setup Carmen as my personal laptop as well as my development environment for my ARMLab research.

## OS

The OS I chose was **Linux Mint 20.2 XFCE** (codename uma) based on Ubuntu 20.04 (Focal Fossa), which is newer than the lab's official 18.04 (Bionic Beaver) Ubuntu development environment.  Some of these instructions are about how to get around that as well as not being exactly on an Ubuntu distribution.

In the 18.04 environment, some packages were too old to be useful or not available at all.  Therefore, I made my own Debian packages to maintain our development environment.  However, in this version, I hope to get everything working with the versions of packages available in the official package repositories, and if not, hopefully someone else has made a PPA I can use rather than rolling my own.

## Update Manager

First thing after installing the OS, I did

```
sudo apt update
sudo apt upgrade
```

After that, I opened the Software Manager gui and chose the fastest mirror.

## Automated Script

I performed many steps after this, but they are both documented and automated (for future use) by the [`carmen-setup.sh`](carmen-setup.sh) script.

## Manual Steps Outside of the Automated Script

Here are the steps taken after all automated steps were executed:

### System Settings

About Me

- Home Phone = 801-842-433
- Email Address = mikebentley15@gmail.com

Appearance

- Style = Mint-Y-Dark
- Fonts > Default Font = Ubuntu Regular, size 11
- Fonts > Default Monospace Font = Monospace Regular, size 11

Desktop

- Background > Wallpaper = `uma_mbrooks_red.jpg`
- Menus > Window List Menu > Show add and remove workspace options in list = False

Workspaces

- General > Layout > Number of workspaces = 9

Panel

- Items > Add = System Load Monitor
- Items > Add = Workspace Switcher
- Items > Window Buttons > Behavior > Middle click action = Nothing
- Items > Window Buttons > Behavior > Draw window frame when hovering a button = True
- Items > Whisker Menu > Appearance > Application icon size = Normal
- Items > System Load Monitor > CPU monitor > Options = (empty)
- Items > System Load Monitor > Memory monitor > Options = (empty)
- Items > System Load Monitor > Swap monitor > Options = (empty)
- Items > System Load Monitor > Uptime monitor = False
- Items > Workspace Switcher > Number of rows = 3

Power Manager

- System > On battery > On critical battery power = Suspend
- Display > On battery > Blank after = 10 minutes
- Display > On battery > Put to sleep after after = Never
- Display > On battery > Switch off after = Never
- Display > Plugged in > Blank after = Never
- Display > Plugged in > Put to sleep after after = Never
- Display > Plugged in > Switch off after = Never

Removable Drives and Media

- Storage > Mount removable drives when hot-plugged = False
- Storage > Mount removable media when inserted = False

Window Manager

- Style > Theme = Mint-Y-Dark
- Style > Title font = Ubuntu Medium, size 11
- Keyboard > Maximize window = Super+Up
- keyboard > Move window to upper workspace = Shift+Ctrl+Alt+Up
- keyboard > Move window to bottom workspace = Shift+Ctrl+Alt+Down
- keyboard > Move window to left workspace = Shift+Ctrl+Alt+Left
- keyboard > Move window to right workspace = Shift+Ctrl+Alt+Right
- keyboard > Tile window to the left = Super+Left
- keyboard -> Tile window to the right = Super+Right

Keyboard

- Application Shortcuts > exo-open --launch WebBrowser = Super+W
- Application Shortcuts > xfce4-popup-whiskermenu = Ctrl+Space


### Application Settings

Firefox:

- General > Startup > Restore previous session = True
- Search > Default Search Engine = DuckDuckGo

Terminator:

- Profiles > Scrolling > Infinite Scrollback = True
- Profiles > Background > Transparent background = 0.90
- Global > Terminal Titlebar > Font color, Focused = dark gray
- Global > Terminal Titlebar > Background, Focused = lime green


### UConnect Wifi

In order to conect to UConnect:

- Connect to UGuest
- In browser, go to https://onboard.utah.edu
- Follow prompts, choosing "Students, Faculty, Staff" > "Laptops, Phones,
  Tablets", and log in
- Choose UConnect (you can try these steps with one of the other options as
  well)
- Click on "Show all operating systems"
- Expand "Other Operating Systems"
- Download the CA Certificate into `~/.certificates` (you may need to create
  the directory).  Get the "CER" version (I haven't tested other versions).
- Download your certificate into `~/.certificates`.  Get the "P12" version (I
  haven't tested other versions).
- Disconnect from UGuest and connect to UConnect.  For me, when I try to
  connect to UConnect, it gives me a pannel of options to fill out:
  - Wi-Fi security: WPA & WPA2 Enterprise
  - Authentication: TLS
  - Identity: u0415196
  - Domain: radius.utah.edu
  - CA certificate: `~/.certificates/CA-*.cer`
  - User certificate: `~/.certificates/u0415196@uconnectutahedu.p12`
  - User key password: **my-unid-password**

Done - I successfully connected to UConnect!


## Other Choices

I choose not to use the system backup tool called **Timeshift**.  When trying
to clean it out in the previous installation, I accidentally borked my system
by doing the equivalent of `rm -rf /` (devastating...).

If I had more disk space than about 140GB, then I would probably use Timeshift, albeit at a limited capacity (like only once weekly and keep only one or two backups).


# Setup Done For Hulk

I installed Xubuntu 18.04 and put hulk as the hostname of the machine.


## Additional Apt Repositories

Open the Software installer gui.  Click on the top-left and select "Software &
Updates".  Under the "Other Software" tab, check "Canonical Partners".


## Installed Google Chrome

Google chrome from
https://www.google.com/chrome/thank-you.html?brand=CHBD&statcb=0&installdataindex=empty&defaultbrowser=0

You download the `.deb` file from that website and then install it with

```bash
sudo apt install ~/Downloads/google-chrome*.deb
```


## Software Installed From Apt:

This list is pretty exhaustive.  Many of these packages are already present on
the system.  This list should be sufficient even for a barebones Ubuntu system,
such as a Docker container.

```bash
sudo apt update
sudo apt upgrade
sudo apt install ...
```

The following packages were installed with `sudo apt install`:

- **apt-transport-https**: Add https support for apt (to connect to gort)
- **aspell**: command-line spell check
- **bash-completion**: for bash completion
- **build-essential**: GNU make and other tools
- **bzr**: Bazaar version control system
- **castxml**: OMPL python bindings dependency
- **chromium-browser**: Chromium browser (open source version of Google Chrome)
- **clang**: Clang C and C++ compiler, default version 6
- **clang-9**: Clang C and C++ compiler, version 9
- **cmake**: CMake meta-build system
- **cmake-curses-gui**: Terminal gui for CMake (called `ccmake`)
- **cppreference-doc-en-html**: C++ standard docs for devhelp
- **cppreference-doc-en-qch**: C++ standard docs for Qt Assistant
- **curl**: Command-line URL manipulator
- **dbus-x11**: D-Bus for X11
- **devhelp**: GNOME developer documentation viewer (I use for C++ docs)
- **dochelp**: Utility to browse system documentation in a web browser
- **docker.io**: Docker container application
- **doxygen**: Documentation generator from source code comments
- **ffmpeg**: Command-line audio and video editor and transcoding
- **ffmpeg-doc**: Documentation for ffmpeg
- **freeglut3-dev**: OMPL App dependency
- **gdb**: GNU command-line debugger
- **gimp**: GNU image manipulation program.  Image editing
- **gimp-data-extras**: Extra brushes and patterns for Gimp
- **gimp-help-en**: Gimp documentation
- **git**: git version control tool
- **gnupg2**: GNU privacy guard.  Create and understand digital signatures
- **google-mock**: Google Mock C++ testing library (similar to Google test)
- **graphviz**: Graph visualization
- **gstreamer1.0-qt5**: Gstreamer sound support for Qt5
- **htop**: Better top viewer for processes
- **imagemagick**: image editing library (with command-line tools)
- **inkscape**: Vector graphics image editor
- **ipython3**: Better interactive python experience
- **kdenlive**: Video editing tool
- **libassimp-dev**: OMPL App dependency
- **libboost-all-dev**: Boost (for ROS 2, OMPL, and odeint)
- **libccd-dev**: Collision detection (used by libfcl-dev)
- **libeigen3-dev**: Eigen3 C++ vector and matrix library
- **libexpat1**: OMPL dependency
- **libfcl-dev**: Collision detection
- **libflann-dev**: OMPL dependency
- **libgtest-dev**: Google test development files for C++
- **libode-dev**: OMPL dependency
- **libpython3-dev**: Development files for compiling against CPython
- **libtinfo5**: OMPL dependency
- **libtriangle-dev**: OMPL dependency
- **locales**: Support for different locales
- **lsb-release**: See the version of OS you are running
- **man-db**: man page viewer
- **mlocate**: The `updatedb` and `locate` command-line tools
- **moreutils**: Many small useful command-line tools (like parallel and vipe)
- **ninja-build**: Ninja build tool (competitor to GNU Make)
- **openssh-server**: SSH server
- **openvpn**: VPN client
- **pkg-config**: Used with CMake for finding non-CMake packages
- **pypy**: Another python interpreter.  Used for OMPL python bindings.
- **python-matplotlib**: Python2 version of matplotlib
- **python-numpy**: Python2 version of numpy
- **python-pygments**: Python2 version of pygmentize
- **python3-argcomplete**: Bash-completion tool for Python with argparse
- **python3-celery**: OMPL App dependency.  Asynchronous job scheduler
- **python3-dev**: Development headers for compiling against CPython
- **python3-flask**: OMPL App dependency.  Micro web server framework
- **python3-matplotlib**: Plotting Python library
- **python3-numpy**: Numerical Python library
- **python3-opengl**: OMPL App dependency.  OpenGL support in Python
- **python3-pip**: Python package manager
- **python3-pyelftools**: Read and write ELF binary files from Python
- **python3-pygments**: Color output from Python.  pygmentize command-line tool
- **python3-pyqt5**: Qt5 bindings from Python
- **python3-pyqt5.qtopengl**: OMPL App dependency.  OpenGL in PyQt5
- **python3-toml**: Reading and writing toml config files from Python
- **qdbus-qt5**: Tools written in Qt for interacting with D-Bus
- **qps**: graphical top-like tool written in Qt
- **qt5-assistant**: Documentation viewer for Qt
- **qt5-default**: Default Qt5 set of modules.  C++ development library
- **qt5-doc**: Qt Assistant version of Qt5 documentation
- **qt5-doc-html**: HTML version of Qt5 documentation
- **qtcreator**: QtCreator C++ editor
- **qtcreator-doc**: documentation for QtCreator
- **ruby**: ruby interpretter
- **ruby-dev**: development files to compiler against ruby
- **screen**: terminal multiplexer
- **software-properties-common**: D-Bus and other abstractions
- **subversion**: svn tool for version control
- **sudo**: to do super user actions
- **tcl-dev**: development files to compile against tcl
- **terminator**: terminal emulator with splitting
- **tk-dev**: development files to compile against tk
- **tmux**: in-terminal multiplexer (more advanced than GNU screen)
- **tree**: tree command to view directory structures
- **ubuntu-restricted-extras**: Additional media codecs and fonts (such as MP3)
- **valgrind**: memory checking debugging tool
- **vim**: vim command-line editor
- **vim-gtk3**: gvim - vim in a graphical window
- **vlc**: Media player (can be used for screen recording)
- **wget**: Simple http retriever
- **x2goclient**: Connect to other computers supporting x2go
- **x2goserver**: Make computer available with x2goclient
- **xscreensaver**: Simple screensaver


## Software Installed From Pip (and others)

```bash
sudo -H pip3 install ...
```

The following packages were installed with `sudo -H pip3 install ...`:

- **pygccxml**: For generating OMPL python bindings
- **pyplusplus**: For generating OMPL python bindings
- **PyOpenGL-accelerate**: OMPL App dependency.  Accelerated OpenGL

The following packages were installed with `sudo gem install ...`:

- **bropages**: Alternative to man.  Short usage examples of cli tools


## Setup Google Test for use

Even though we install Google Test developer files through apt, it is not
compiled.  It just installs the source code into `/usr/src/gtest`.  Here, we go
there and compile it as root and install the generated static libraries.

```
cd /usr/src/gtest
sudo mkdir build
cd build
sudo cmake .. -DCMAKE_BUILD_TYPE=Release
sudo make
sudo cp *.a /usr/lib/
```


## Setup Screensaver

Get xscreensaver to start by default with the system.

```bash
sudo sed -i -e 's,xscreensaver,/usr/bin/xscreensaver,g' \
  /usr/lib/systemd/user/xscreensaver.service
sudo systemctl enable /usr/lib/systemd/user/xscreensaver.service
```


## Install Visual Studio Code

Visual Studio Code is a free lightweight cross-platform code editor made by
Microsoft.  Very high quality.

```bash
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | \
  sudo apt-key add -
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" | \
  sudo tee /etc/apt/sources.list.d/vscode.list
sudo apt update
sudo apt install code
```


## Install Skype

```bash
wget https://repo.skype.com/latest/skypeforlinux-64.deb
sudo apt install ./skypeforlinux-64.deb
rm ./skypeforlinux-64.deb
```


## Install ROS 2

### Add the ROS 2 repository

```bash
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
  sudo apt-key add -
echo "deb [arch=amd64] http://packages.ros.org/ros2/ubuntu bionic main" | \
  sudo tee /etc/apt/sources.list.d/ros2-latest.list
sudo apt update
```


### Download and unpack ROS 2

```bash
wget https://github.com/ros2/ros2/releases/download/release-eloquent-20200124/ros2-eloquent-20200124-linux-bionic-amd64.tar.bz2
sudo tar --bzip2 -xvf \
  ros2-eloquent-20200124-linux-bionic-amd64.tar.bz2 \
  --directory /opt/
sudo mkdir /opt/ros
sudo mv /opt/ros2-linux /opt/ros/eloquent
rm ros2-eloquent-20200124-linux-bionic-amd64.tar.bz2
```

ROS 2 setup script to be sourced.  Create a file `/etc/profile.d/ros2.sh

```bash
if [ -f "/opt/ros/eloquent/setup.bash" ]; then
  source "/opt/ros/eloquent/setup.bash"
fi
```


### Install rosdep and missing dependencies

```bash
sudo apt install python-rosdep
sudo rosdep init
rosdep update
rosdep install \
  --from-paths /opt/ros/eloquent/share \
  --ignore-src \
  --rosdistro eloquent \
  -y \
  --skip-keys "console_bridge fastcdr fastrtps libopensplice67 libopensplice69 osrf_testing_tools_cpp poco_vendor rmw_connext_cpp rosidl_typesupport_connext_c rosidl_typesupport_connext_cpp rti-connext-dds-5.3.1 tinyxml_vendor tinyxml2_vendor urdfdom urdfdom_headers"
```

This `rosdep install` command will call `sudo apt install ...` for many
packages that are missing and needed by ROS 2.


### Setup colcon build

ROS 2 uses `colcon` instead of `catkin`.

```bash
sudo apt install python3-colcon-common-extensions
```


### Install connext DDS

This is an optional transport layer used by ROS 2.  It is commercial software
that has a free private use license.  I do not know if we will even use it, but
since the binary installed ROS 2 was compiled against it, this is the easiest
way to remove the warnings that ROS 2 generates if it is missing at runtime.

```bash
sudo apt update
sudo apt install rti-connext-dds-5.3.1
```

Then create the file `/etc/profile.d/connext.sh` with following contents

```bash
export RTI_LICENSE_FILE=/opt/rti.com/rti_connext_dds-5.3.1/rti_license.dat
if [ -f "/opt/rti.com/rti_connext_dds-5.3.1/resource/scripts/rtisetenv_x64Linux3gcc5.4.0.bash" ]; then
  cd /opt/rti.com/rti_connext_dds-5.3.1/resource/scripts
  source ./rtisetenv_x64Linux3gcc5.4.0.bash > /dev/null
  cd - > /dev/null
fi
```


## Add Extra SSH Port

Since at the U port 22 is closed, we open up another port to be accessible from
off-campus (they recommend port 5522).

Open `/etc/ssh/sshd_config`, and on line 13 it should have

```
#Port 22
```

Change this line to read

```
Port 22
Port 5522
```

This will make the SSH server listen to both ports 22 and 5522.

Restart the ssh server

```bash
sudo systemctl restart sshd
```


## Download and install OMPL

Download the OMPL app

```bash
cd /opt
sudo git clone https://github.com/ompl/omplapp.git
cd omplapp
sudo git checkout e2aa31
sudo git clone https://github.com/ompl/ompl.git
cd ompl
sudo git checkout adc802
```

Build and install ompl

```bash
wget http://ompl.kavrakilab.org/install-ompl-ubuntu.sh
sudo bash -x ./install-ompl-ubuntu.sh --app
rm install-ompl-ubuntu.sh
```


## Deb Builder

Rename my group to be the lab's group

```bash
sudo groupmod --new-name lab-continuum mbentley
```

Create a group for being able to do sudo without a password (not yet used)

```bash
sudo groupadd sudo-nopw
echo "%sudo-nopw ALL=(ALL) NOPASSWD: ALL" | \
  sudo tee /etc/sudoers.d/sudo-nopw-group
```

Create the user `deb-builder` for building debian packages

```bash
sudo useradd \
  --uid 2345 \
  --create-home \
  --home-dir /home/deb-builder \
  --shell /bin/bash \
  --no-user-group \
  --gid lab-continuum \
  --groups sudo \
  deb-builder
sudo ln -s /home/deb-builder /var/
```

## Install PyCharm

- Download the latest tar from https://www.jetbrains.com/pycharm/
- untar this into `/opt` and rename the expanded directory to `/opt/pycharm`

```
sudo ln -s /opt/pycharm/bin/pycharm.sh /usr/local/bin/pycharm
cat <<EOF | sudo tee /usr/share/applications/pycharm.desktop
[Desktop Entry]
Name=PyCharm
Comment=The Python IDE for Professional Developers
Exec=/opt/pycharm/bin/pycharm.sh
Icon=/opt/pycharm/bin/pycharm.png
Terminal=false
Type=Application
Categories=Development;
EOF
```


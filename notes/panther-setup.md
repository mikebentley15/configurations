# Panther Setup

I installed Ubuntu 18.04 on this machine and put its hostname as panther
(as in the black panther)

## initial installation

Installed Ubuntu 18.04.

- allowed to update during install
- allowed installing 3rd party apps and drivers
- 200GB partition from M.2 drive at "/"
- 800GB partition from M.2 drive at "/home"
- 1TB partition from SSD at "/mnt/p"
- 4TB partition from HDD at "/mnt/q"

## additional apt repositories

Open the Software installer gui.  Click on the top-left and select "Software &
Updates".  Under the "Other Software" tab, check "Canonical Partners".

## initial update

For some reason, even though I asked the installer to update everything,
software is still out of date.  I simply did

```bash
sudo apt update
sudo apt upgrade
```

## install google chrome

Go to https://www.google.com/chrome

download the .deb file and install with

```bash
sudo apt install ~/Downloads/google-chrome*.deb
```

## autoremove unused packages

For some reason, a base install had some extra things installed that were not
necessary after doing an `apt update`.  Let's remove them.

```bash
sudo apt autoremove
```

## request to get machine on the network

I sent an email to support@cs.utah.edu with the following contents:

    Hello Support,

    We have built a new computer and would like to connect it to the network.
    The hostname we have chosen (for now) is "panther", so
    "panther.cs.utah.edu".

    hostname: panther.cs.utah.edu
    room: MEB 3375
    ethernet port: 3138 A
    mac address: 2C:F0:5D:55:31:11
    owner: Alan Kuntz

    The machine is currently connected to the internet using the UGuest wifi.

    Thanks,
    Michael Bentley

## basic installation (not everything)

```bash
sudo apt update
sudo apt install -y \
  apt-transport-https \
  aspell \
  bash-completion \
  build-essential \
  bzr \
  castxml \
  chromium-browser \
  clang \
  clang-9 \
  cmake \
  cmake-curses-gui \
  cppreference-doc-en-html \
  cppreference-doc-en-qch \
  curl \
  dbus-x11 \
  devhelp \
  dochelp \
  docker.io \
  doxygen \
  emacs \
  ffmpeg \
  ffmpeg-doc \
  freeglut3-dev \
  gdb \
  gimp \
  gimp-data-extras \
  gimp-help-en \
  git \
  gnupg2 \
  google-mock \
  graphviz \
  gstreamer1.0-qt5 \
  htop \
  imagemagick \
  inkscape \
  ipython3 \
  kdenlive \
  libassimp-dev \
  libboost-all-dev \
  libccd-dev \
  libeigen3-dev \
  libexpat1 \
  libfcl-dev \
  libflann-dev \
  libgtest-dev \
  libode-dev \
  libpython3-dev \
  libtinfo5 \
  libtriangle-dev \
  locales \
  lsb-release \
  man-db \
  mlocate \
  moreutils \
  neovim \
  ninja-build \
  openssh-server \
  openvpn \
  pkg-config \
  pypy \
  python-matplotlib \
  python-numpy \
  python-pygments \
  python3-argcomplete \
  python3-celery \
  python3-dev \
  python3-flask \
  python3-matplotlib \
  python3-numpy \
  python3-opengl \
  python3-pip \
  python3-pyelftools \
  python3-pygments \
  python3-pyqt5 \
  python3-pyqt5.qtopengl \
  python3-toml \
  qdbus-qt5 \
  qps \
  qt5-assistant \
  qt5-default \
  qt5-doc \
  qt5-doc-html \
  qtcreator \
  qtcreator-doc \
  ruby \
  ruby-dev \
  screen \
  software-properties-common \
  subversion \
  sudo \
  tcl-dev \
  terminator \
  tk-dev \
  tmux \
  tree \
  ubuntu-restricted-extras \
  valgrind \
  vim \
  vim-gtk3 \
  vlc \
  wget \
  x2goclient \
  x2goserver \
  xscreensaver
```

## software installed from pip (and others)

```bash
sudo -H pip3 install \
  pygccxml \
  pyplusplus \
  PyOpenGL-accelerate
```

```bash
sudo gem install bropages
```

## add extra ssh port

Since at the U, port 22 is closed, we open up another port to be accessible
from off-campus (they recommend port 5522).

Open `/etc/ssh/sshd_config`, and on line 13, it should have

```
#Port 22
```

change this line to read

```
Port 22
Port 5522
```

This will make the SSH server listen to both ports 22 and 5522.

Restart the ssh server

```bash
sudo systemctl restart sshd
```

## setup google test for use

even though we install Google Test developer files through apt, it is not
compiled.  It just installs the source code into `/usr/src/gtest`.  Here, we go
there and compile it as root and install the generated static libraries.

```bash
cd /tmp
mkdir gtest-build
cd gtest-build
cmake /usr/src/gtest -DCMAKE_BUILD_TYPE=Release
make
sudo cp *.a /usr/lib
cd ..
rm -rf gtest-build
```

## setup screensaver

Get xscreensaver to start by default with the system

```bash
sudo sed -i -e 's,xscreensaver,/usr/bin/xscreensaver,g' \
  /usr/lib/systemd/user/xscreensaver.service
sudo systemctl enable /usr/lib/systemd/user/xscreensaver.service
```

## install visual studio code

Visual Studio Code is a free lightweight cross-platform code editor made by
Microsoft.  Very high quality.

```bash
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | \
  sudo apt-key add -
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" | \
  sudo tee /etc/apt/sources.list.d/vscode.list
sudo apt update
sudo apt install code
```

## install skype

```bash
wget https://repo.skype.com/latest/skypeforlinux-64.deb
sudo apt install ./skypeforlinux-64.deb
rm ./skypeforlinux-64.deb
```

## install zoom

**todo**

## install ros2

### Add the ROS 2 repository

```bash
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
  sudo apt-key add -
echo "deb [arch=amd64] http://packages.ros.org/ros2/ubuntu bionic main" | \
  sudo tee /etc/apt/sources.list.d/ros2-latest.list
sudo apt update
```

### Install much of ROS 2

```bash
sudo apt install ros-eloquent-desktop
```


## todo

[ ] make a pseudo package that installs extra repositories for apt
[ ] make a pseudo package containing the full list of things to install
[ ] install this pseudo package
[x] after getting network access, request port 5522 be opened for ssh access
[ ] test development environment
[ ] install nvidia driver from the nvidia install script
[x] fix the keyboard (hardware)
[x] install the cdrom using another sata cable
[x] install visual studio code
[ ] install skype
[ ] install zoom


# Installed Stuff via apt

Using the `apt-mark showmanual`, we can get the list of all packages manually
installed.  This will also include all packages installed initially with
Ubuntu.  We cal filter out those packages with:

```bash
comm -23 \
  <(apt-mark showmanual | sort -u) \
  <(gzip -dc /var/log/installer/initial-status.gz | \
      sed -n 's/^Package: //p' | sort -u)
```

which outputs

```
apt-transport-https
armlab-cisst
build-essential
bzr
castxml
chromium-browser
clang
clang-9
cmake
cmake-curses-gui
cmake-qt-gui
code
cppreference-doc-en-html
cppreference-doc-en-qch
curl
devhelp
dochelp
docker.io
doxygen
elinks
emacs
expect
ffmpeg
ffmpeg-doc
freeglut3-dev
g++
gedit
gedit-plugins
gfortran
gimp
gimp-data-extras
gimp-help-en
git
gitk
gnome-session-bin
gnupg2
google-chrome-stable
google-mock
graphviz
gstreamer1.0-qt5
htop
inkscape
ipython3
kdenlive
libassimp-dev
libboost-all-dev
libboost-filesystem-dev
libboost-numpy-dev
libboost-program-options-dev
libboost-python-dev
libboost-serialization-dev
libboost-system-dev
libboost-test-dev
libccd-dev
libcppunit-dev
libdrm-dev
libeigen3-dev
libexpat1-dev
libf2c2-dev
libfcl-dev
libflann-dev
libglw1-mesa
libglw1-mesa-dev
libgtest-dev
libmotif-dev
libncurses5-dev
libode-dev
libpython3-dev
libqt5serialport5-dev
libqt5xmlpatterns5-dev
libraw1394-dev
libspdlog-dev
libtriangle-dev
libx11-dev
libxdamage-dev
libxext-dev
libxml2-dev
libxt-dev
libxxf86vm-dev
libyaml-cpp-dev
linuxbrew-wrapper
ll4ma-ompl
moreutils
neovim
ninja-build
openssh-server
openvpn
pkg-config
pypy
python3-argcomplete
python3-celery
python3-colcon-common-extensions
python3-dev
python3-flask
python3-matplotlib
python3-mock
python3-numpy
python3-opencv
python3-opengl
python3-pip
python3-pyelftools
python3-pygments
python3-pygraphviz
python3-pyqt5
python3-pyqt5.qtopengl
python3-pytest-mock
python3-toml
python-catkin-tools
python-matplotlib
python-numpy
python-pip
python-pygments
python-rosdep
python-rosinstall
python-rosinstall-generator
python-wstool
qdbus-qt5
qps
qt5-assistant
qt5-default
qt5-doc
qt5-doc-html
qt5serialport-examples
qtcreator
qtcreator-doc
ros-eloquent-ament-clang-format
ros-eloquent-ament-clang-tidy
ros-eloquent-ament-cmake
ros-eloquent-ament-cmake-auto
ros-eloquent-ament-cmake-clang-format
ros-eloquent-ament-cmake-clang-tidy
ros-eloquent-ament-cmake-copyright
ros-eloquent-ament-cmake-core
ros-eloquent-ament-cmake-cppcheck
ros-eloquent-ament-cmake-cpplint
ros-eloquent-ament-cmake-export-definitions
ros-eloquent-ament-cmake-export-dependencies
ros-eloquent-ament-cmake-export-include-directories
ros-eloquent-ament-cmake-export-interfaces
ros-eloquent-ament-cmake-export-libraries
ros-eloquent-ament-cmake-export-link-flags
ros-eloquent-ament-cmake-flake8
ros-eloquent-ament-cmake-gmock
ros-eloquent-ament-cmake-gtest
ros-eloquent-ament-cmake-include-directories
ros-eloquent-ament-cmake-libraries
ros-eloquent-ament-cmake-lint-cmake
ros-eloquent-ament-cmake-mypy
ros-eloquent-ament-cmake-nose
ros-eloquent-ament-cmake-pclint
ros-eloquent-ament-cmake-pep257
ros-eloquent-ament-cmake-pep8
ros-eloquent-ament-cmake-pyflakes
ros-eloquent-ament-cmake-pytest
ros-eloquent-ament-cmake-python
ros-eloquent-ament-cmake-ros
ros-eloquent-ament-cmake-target-dependencies
ros-eloquent-ament-cmake-test
ros-eloquent-ament-cmake-uncrustify
ros-eloquent-ament-cmake-version
ros-eloquent-ament-cmake-virtualenv
ros-eloquent-ament-cmake-xmllint
ros-eloquent-ament-copyright
ros-eloquent-ament-cppcheck
ros-eloquent-ament-cpplint
ros-eloquent-ament-flake8
ros-eloquent-ament-index-cpp
ros-eloquent-ament-index-cpp-dbgsym
ros-eloquent-ament-index-python
ros-eloquent-ament-lint
ros-eloquent-ament-lint-auto
ros-eloquent-ament-lint-cmake
ros-eloquent-ament-lint-common
ros-eloquent-ament-mypy
ros-eloquent-ament-package
ros-eloquent-ament-pclint
ros-eloquent-ament-pep257
ros-eloquent-ament-pep8
ros-eloquent-ament-pyflakes
ros-eloquent-ament-uncrustify
ros-eloquent-ament-virtualenv
ros-eloquent-ament-xmllint
ros-eloquent-desktop
ros-eloquent-launch-testing
ros-eloquent-launch-testing-ament-cmake
ros-eloquent-launch-testing-ros
ros-eloquent-osrf-testing-tools-cpp
ros-eloquent-rmw-connext-cpp
ros-eloquent-rmw-cyclonedds-cpp
ros-eloquent-rmw-opensplice-cpp
ros-eloquent-ros2lifecycle-test-fixtures
ros-eloquent-rosidl-typesupport-connext-c
ros-eloquent-rosidl-typesupport-connext-cpp
ros-eloquent-rosidl-typesupport-opensplice-c
ros-eloquent-rosidl-typesupport-opensplice-cpp
ros-eloquent-ros-testing
ros-eloquent-rviz-rendering-tests
ros-eloquent-rviz-visual-testing-framework
ros-eloquent-spdlog-vendor
ros-eloquent-test-interface-files
ros-eloquent-test-msgs
ros-melodic-desktop
ros-melodic-desktop-full
ruby
ruby-dev
screen
skypeforlinux
slack-desktop
sox
subversion
swig
tcl-dev
tcsh
terminator
tk-dev
tmux
tree
ubuntu-restricted-extras
valgrind
vim
vim-gtk3
vlc
x11proto-dri2-dev
x11proto-gl-dev
x11proto-print-dev
x2goclient
x2goserver
xscreensaver
```

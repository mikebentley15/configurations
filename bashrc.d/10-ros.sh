
# don't source ros if we have an active conda environment
if [[ -v CONDA_DEFAULT_ENV ]]; then
  # include the first available ros package, starting from ros2, then ros1
  for distro in rolling galactic foxy eloquent dashing crystal bouncy ardent noetic melodic lunar kinetic jade indigo; do
    if [ -e "/opt/ros/${distro}/setup.bash" ]; then
      source "/opt/ros/${distro}/setup.bash"
      break
    fi
  done
fi

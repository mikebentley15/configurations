# prepares the environment for use with the tendon_experiments repo
function tendonenv() {
  pathmunge PATH ~/git/armlab/tendon_experiments/build
  pathmunge PYTHONPATH ~/git/armlab/tendon_experiments/build
}
export -f tendonenv

#
# This file contains convenience aliases
#

# if slurm is installed, use slurm aliases
if which squeue &>/dev/null; then
  source ~/.slurm_aliases 2>/dev/null || true
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
  #alias dir='dir --color=auto'
  #alias vdir='vdir --color=auto'

  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

if which matlab &>/dev/null; then
  alias matlabcli='matlab -nodesktop -nodisplay -nojvm -nosplash'
fi

alias in-docker="awk -F/ \'\$2 == \"docker\"\' /proc/self/cgroup | read"


# default to python3. You can still call python2 if that's what you really want
if ! which python2 &>/dev/null; then
  alias python2="\\python"
fi
if ! which pylint2 &>/dev/null; then
  alias pylint2="\\pylint"
fi
if ! which ipython2 &>/dev/null; then
  alias ipython2="\\ipython"
fi
alias python="python3"
alias ipython="ipython3"
if which pylint3 &>/dev/null; then
  alias pylint="pylint3"
fi


# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias randpw='dd if=/dev/urandom bs=1 count=24 2>/dev/null | base64'

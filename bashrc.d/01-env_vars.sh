# ANSI color codes
RS="\033[0m"    # reset
HC="\033[1m"    # hicolor
UL="\033[4m"    # underline
INV="\033[7m"   # inverse background and foreground
FBLK="\033[30m" # foreground black
FRED="\033[31m" # foreground red
FGRN="\033[32m" # foreground green
FYEL="\033[33m" # foreground yellow
FBLE="\033[34m" # foreground blue
FMAG="\033[35m" # foreground magenta
FCYN="\033[36m" # foreground cyan
FWHT="\033[37m" # foreground white
BBLK="\033[40m" # background black
BRED="\033[41m" # background red
BGRN="\033[42m" # background green
BYEL="\033[43m" # background yellow
BBLE="\033[44m" # background blue
BMAG="\033[45m" # background magenta
BCYN="\033[46m" # background cyan
BWHT="\033[47m" # background white
# escaped version for use in PS prompt variables
ERS="\[${RS}\]"     # reset
EHC="\[${HC}\]"     # hicolor
EUL="\[${UL}\]"     # underline
EINV="\[${INV}\]"   # inverse background and foreground
EFBLK="\[${FBLK}\]" # foreground black
EFRED="\[${FRED}\]" # foreground red
EFGRN="\[${FGRN}\]" # foreground green
EFYEL="\[${FYEL}\]" # foreground yellow
EFBLE="\[${FBLE}\]" # foreground blue
EFMAG="\[${FMAG}\]" # foreground magenta
EFCYN="\[${FCYN}\]" # foreground cyan
EFWHT="\[${FWHT}\]" # foreground white
EBBLK="\[${BBLK}\]" # background black
EBRED="\[${BRED}\]" # background red
EBGRN="\[${BGRN}\]" # background green
EBYEL="\[${BYEL}\]" # background yellow
EBBLE="\[${BBLE}\]" # background blue
EBMAG="\[${BMAG}\]" # background magenta
EBCYN="\[${BCYN}\]" # background cyan
EBWHT="\[${BWHT}\]" # background white

BASH_CLEAR="$(tput sgr0)"
BASH_BOLD="$(tput bold)"
BASH_BLACK="$(tput setaf 0)"
BASH_RED="$(tput setaf 1)"
BASH_GREEN="$(tput setaf 2)"
BASH_YELLOW="$(tput setaf 3)"
BASH_BLUE="$(tput setaf 4)"
BASH_PURPLE="$(tput setaf 5)"
BASH_CYAN="$(tput setaf 6)"
BASH_WHITE="$(tput setaf 7)"
BASH_BROWN="$(tput setaf 94)"
BASH_GRAY="$(tput setaf 245)"
BASH_DARKGRAY="$(tput setaf 240)"
BASH_LIGHTGRAY="$(tput setaf 250)"
BASH_GREY="${BASH_GRAY}"
BASH_DARKGREY="${BASH_DARKGRAY}"
BASH_LIGHTGREY="${BASH_LIGHTGRAY}"

export PS_DARK=0

function _prompt_err_code() {
  local code=$1
  echo -en "${RS}${HC}"
  if [ ${code} -ne 0 ]; then
    echo -en "[${FRED}]( ${code} )" # background red, foreground white
  else
    echo -en "[${FCYN}](${code})"
  fi
  echo -en "[${RS}]"
  return ${code}
}
export -f _prompt_err_code

# function to generate the PS1 prompt correctly
function _bentley_prompt() {
  local err="${?##0}"
  local name="Auto"
  if [ -f /etc/docker-name ]; then
    name="$(cat /etc/docker-name)"
  fi

  local col_err="${EFRED}"
  local col_host="${EFYEL}"
  local col_prompt="${EFGRN}"
  local col_conda="${EFGRN}"
  if [ ${PS_DARK} -eq 0 ]; then
    col_host="${EFBLE}"
    col_prompt="${EFBLK}"
    col_conda="${EFBLK}"
  fi

  local err_msg=""
  if [ "${err}" ]; then
    err_msg="${EHC}${col_err} error ${err} >${ERS}"
  fi

  local conda_env=""
  if [ "${CONDA_DEFAULT_ENV}" ]; then
    conda_env=" ${col_conda}${CONDA_DEFAULT_ENV}"
  fi

  export PS1="${err_msg}${conda_env}${EHC}${col_host} (${name})${col_prompt} \! \W \$${ERS} "
}
export -f _bentley_prompt
export PROMPT_COMMAND=_bentley_prompt

# Set the prompt variable
function toggle_dark() {
  if [ ${PS_DARK} -eq 1 ]; then
    export PS_DARK=0
    export VIM_COLOR=zellner
  else
    export PS_DARK=1
    export VIM_COLOR=elflord
  fi
}
export -f toggle_dark

toggle_dark
#PS1='\[\e[1;\]\h \! \W \$\[\e[0m\] '
#PS1='\[\e[1;37m\]\h \! \W $\[\e[0m\] '
#if in-docker; then
#  if [ -f "/etc/docker-name" ]; then
#    _name="$(cat /etc/docker-name)"
#  else
#    _name="docker"
#  fi
#  PS1="${EHC}${EFYEL} (${_name})${ERS}$PS1"
#  unset _name
#fi

# needs 00-aliases to be imported first

# Add to the path variable
pathmunge PATH /usr/sbin
pathmunge PATH /sbin
pathmunge PATH /usr/local/cuda/bin
pathmunge PATH /opt/intel/bin
pathmunge PATH /opt/cling/bin
#pathmunge PATH $HOME/android-sdk/sdk/tools
#pathmunge PATH $HOME/android-sdk/sdk/platform-tools

#pathpremunge PATH $HOME/anaconda3/bin
#pathpremunge PATH /opt/anaconda3/bin
pathpremunge PATH $HOME/.local/share/gem/ruby/3.0.0/bin
pathpremunge PATH $HOME/git/spack/bin
pathpremunge PATH $HOME/.local/bin
pathpremunge PATH $HOME/apps/bin
pathpremunge PATH $HOME/apps/usr/bin
pathpremunge PATH $HOME/bin

# This allows Songbird to play aac files
#SB_GST_SYSTEM=1

# Define the default command-line editor
EDITOR=vim
export EDITOR

YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"
export YAOURT_COLORS

VISUAL=vim
export VISUAL

AWS_PROFILE=shared-services
export AWS_PROFILE

# vim modelines
# vim: set ft=sh:

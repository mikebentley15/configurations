#
# This file contains convenience bash functions
#

function _pathmunge_impl() {
  local type="$1"
  local varname="$2"
  local toadd="$3"
  if [ -d "$toadd" ]; then
    case "${!varname}" in
      *$toadd*)
        ;;
      *)
        case "$type" in
          pre)
            export ${varname}="$toadd:${!varname}"
            ;;
          post)
            export ${varname}="${!varname}:$toadd"
            ;;
          *)
            echo "Warning: unsupported pathmunge type: $type" >&2
            ;;
        esac
        ;;
    esac
  fi
}

function pathmunge() {
  _pathmunge_impl post "$@"
}
export -f pathmunge

function pathpremunge() {
  _pathmunge_impl pre "$@"
}
export -f pathpremunge

#
# Convenience aliases and functions for opening files using default apps
# - open
# - default-type
# - default-app
# - default-set
#
if [ -x /usr/bin/xdg-open ]; then
  function open() {
    # check that there is an argument
    if [ $# -eq 0 ]; then
      echo "Missing arguments, try --help for more information" >&2
      return 1
    fi

    # check for flags
    case "$1" in
      --help)
        echo "Usage:"
        echo "  open {file | URL} [{file | URL} ...]"
        echo "  open {--help | --manual | --version"
        echo
        echo "Description:"
        echo "  Opens each file using the default application.  Uses the"
        echo "  xdg-open command to do so."
        echo
        xdg-open --help
        return $?
        ;;

      --*)
        xdg-open "$1"
        return $?
        ;;

      *) # default: move on
        ;;
    esac

    while [ $# -gt 0 ]; do
      xdg-open "$1"
      shift
    done
  }
  export -f open

  function default-type() {
    if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
      echo "Usage: default-type <file>"
      echo
      echo "Prints out the recognized file type for <file>"
      return 0
    fi
    xdg-mime query filetype $1
  }
  export -f default-type

  function default-app() {
    if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
      echo "Usage: default-app <file>"
      echo
      echo "Prints out the default app for opening <file> using open"
      return 0
    fi
    if [ $# -lt 1 ]; then
      echo "Not enough arguments. Use --help for more info" >&2
      return 1
    fi
    local filetype="$(xdg-mime query filetype $1)"
    # output the default app
    xdg-mime query default ${filetype}
  }
  export -f default-app

  function default-set() {
    if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
      echo "Usage: default-set <file> <desktop-file>"
      echo
      echo "Sets the default application for all files matching the type of"
      echo "<file> to <desktop-file>."
      echo
      echo "  <file>           A file with a recognized mime type (using default-type)"
      echo
      echo "  <desktop-file>   A file ending in '.desktop' containing information of"
      echo "                   the app to use.  You can use 'locate' to try to find the"
      echo "                   appropriate desktop file for the application you want to"
      echo "                   use (e.g., 'locate .desktop | grep -i inkscape')."
      return 0
    fi
    if [ $# -lt 2 ]; then
      echo "Not enough arguments. Use --help for more info" >&2
      return 1
    fi
    local file="$1"
    local desktopfile="$2"
    local filetype="$(xdg-mime query filetype ${file})"
    xdg-mime default "${desktopfile}" "${filetype}"
  }
  export -f default-set

fi

function pdfopen() {
  okular "$@" &>/dev/null &
}
export -f pdfopen

function dockertags(){
  if [ $# -lt 1 ]; then
    echo "dockertags: list all tags for a Docker image on a remote registry."
    echo
    echo "Examples:"
    echo "  - List all tags for ubuntu:"
    echo "    dockertags ubuntu"
    echo "  - List all php tags containing 'apache':"
    echo "    dockertags php apache"
    echo
    return 1
  fi
  local image="$1"
  local filter="$2"
  wget -q \
    https://registry.hub.docker.com/v1/repositories/${image}/tags \
    -O - | \
      sed \
        -e 's/[][]//g' \
        -e 's/"//g' \
        -e 's/ //g' |
      tr '}' '\n' |
      awk -F: '{print $3}' |
      grep "$filter"
}
export -f dockertags

function colors() {
  local fgc bgc vals seq0

  printf "Color escapes are %s\n" '\e[${value};...;${value}m'
  printf "Values 30..37 are \e[33mforeground colors\e[m\n"
  printf "Values 40..47 are \e[43mbackground colors\e[m\n"
  printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

  # foreground colors
  for fgc in {30..37}; do
    # background colors
    for bgc in {40..47}; do
      fgc=${fgc#37} # white
      bgc=${bgc#40} # black

      vals="${fgc:+$fgc;}${bgc}"
      vals=${vals%%;}

      seq0="${vals:+\e[${vals}m}"
      printf "  %-9s" "${seq0:-(default)}"
      printf " ${seq0}TEXT\e[m"
      printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
    done
    echo; echo
  done
}
export -f colors

#
# ex - archive extractor
# usage: ex <file>
function ex () {
  if [ -f "$1" ] ; then
    case "$1" in
      *.tar.bz2)   tar xjf "$1"   ;;
      *.tar.gz)    tar xzf "$1"   ;;
      *.bz2)       bunzip2 "$1"   ;;
      *.rar)       unrar x "$1"   ;;
      *.gz)        gunzip "$1"    ;;
      *.tar)       tar xf "$1"    ;;
      *.tbz2)      tar xjf "$1"   ;;
      *.tgz)       tar xzf "$1"   ;;
      *.zip)       unzip "$1"     ;;
      *.Z)         uncompress "$1";;
      *.7z)        7z x "$1"      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
export -f ex

#
# Counts down and optionally executes a command
#
# @arg time: in seconds
# @arg command: the rest of the command line arguments to execute afterwards
#
function countdown() {
  if [ "$#" -eq 0 ]; then
    echo "Error: you must provide a time" >&2
    echo "Usage: countdown <seconds> [<command>]" >&2
    return 1
  fi
  local secs="$1"
  local date1=$((`date +%s` + $secs))
  shift
  # Show the countdown
  echo -ne "\r$(date -u --date @$secs +%H:%M:%S)"
  sleep 1.1
  while [ "$date1" -ge $(date +%s) ]; do
    echo -ne "\r$(date -u --date @$(($date1 - `date +%s` + 1)) +%H:%M:%S)"
    sleep 0.1
  done
  echo -e "\r$(date -u --date @0 +%H:%M:%S)"
  # Execute the command if given
  if [ "$#" -gt 0 ]; then
    "$@"
  fi
}
export -f countdown

#
# Shows a stopwatch timer
#
function stopwatch() {
  date1=`date +%s`;
  while true; do
    echo -ne "\r$(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)"
    sleep 0.1
  done
  echo
}
export -f stopwatch

function myspeak() {
  espeak-ng -a 180 -s 130 -v mb-us1 "$@"
}

# activate the PlatformIO environment
function pio_activate() {
  source ~/.platformio/penv/bin/activate
}

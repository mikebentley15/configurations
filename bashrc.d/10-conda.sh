# try to include miniconda, else try anaconda
__conda=""
if   [ -x "$HOME/miniconda3/bin/conda" ]; then
  __conda="$HOME/miniconda3/bin/conda"
elif [ -x "$HOME/anaconda3/bin/conda" ]; then
  __conda="$HOME/anaconda3/bin/conda"
fi

if [ -n "$__conda" ]; then
  __conda_setup="$("$__conda" shell.bash hook 2> /dev/null)"
  __conda_dir="$(dirname "$(dirname "$__conda")")"
  if [ $? -eq 0 ]; then
    eval "$__conda_setup"
  elif [ -f "$__conda_dir/etc/profile.d/conda.sh" ]; then
    source "$__conda_dir/etc/profile.d/conda.sh"
  else
    pathpremunge PATH "$__conda_dir/condabin"
  fi
  unset __conda_setup
  unset __conda_dir
fi
unset __conda

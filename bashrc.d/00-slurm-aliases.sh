function is_kingspeak() {
  [[ $(hostname) =~ kingspeak*|kp* ]]
}

# Shows all jobs queued or running that you started
alias s-mine='squeue --format="%.16i %.20P %.20q %.33j %.11T %.10M %.11l %.5D %.8Q %.19S %R" -u $USER'

# Shows all jobs queued or running which have requested the soc-gpu-kp resources
alias s-ours='squeue --format="%.16i %.15q %.33j %.8u %.8T %.10M %.11l %.5D %.8Q %.19S %R" -p soc-gpu-kp'

# Shows all account and partition pairings you are part of
alias s-accts='printf "%-15s%-25s%s\n" "Cluster" "Account" "Partition" && sacctmgr -p show assoc user=$USER | awk -F"|" "NR>1 { printf \"%-15s%-25s%s\n\", \$1, \$2, \$18 }" | sort'

alias s-i="sinfo -o \"%20P %5D %14F %8z %10m %10d %11l %16f %N\""
alias s-i2="sinfo -o \"%20P %5D %6t %8z %10m %10d %11l %16f %N\""
alias s-q="squeue -o \"%8i %12j %4t %10u %20q %20a %10g %20P %10Q %5D %11l %R\""

if is_kingspeak; then
  alias s-irun='srun --time=8:00:00 --nodes=1 --ntasks=1 --cpus-per-task=28 --account=ganesh-kp --partition=soc-kp --pty /bin/bash -l'
fi

if is_kingspeak && which singularity &>/dev/null; then
  alias sing-exec="singularity exec $HOME/singularity/archflit-mfem.simg"
fi

# vim modelines
# vim: set ft=sh:

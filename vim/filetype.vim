" user filetype file
if exists("did_load_filetypes")
  finish
endif

augroup filetypedetect

au! BufNewFile,BufRead *.applescript                 setf applescript
au! BufNewFile,BufRead *.gplt *.plot *.plt,*.gnuplot setf gnuplot
au! BufNewFile,BufRead .tags                         setf tags
au! BufNewFile,BufRead *.cu *.cuh *.cuxx             setf cu
au! BufNewFile,BufRead *.gcov                        setf gcov

augroup END

" Run clang-format on the current buffer only if .clang-format exists within
" the direct folder parentage.
function ClangFormat()
  if !empty(findfile('.clang-format', expand('%:p:h') . ';'))
    let cursor_pos = getpos('.')
    :%!clang-format --assume-filename '%:p'
    call setpos('.', cursor_pos)
  endif
endfunction

function ClangFormatIfModified()
  if &modified
    call ClangFormat()
  endif
endfunction

function TrunkFormat()
  let trunk = findfile('trunk', expand('%:p:h') . ';')
  if &modified
    :w
  endif
  if !empty(trunk)
    let curfile = expand('%:p')
    let cursor_pos = getpos('.')
    exec ':!' . trunk . ' fmt %:p'
    call setpos('.', cursor_pos)
  endif
endfunction

command TrunkFormat :call TrunkFormat()
command ClangFormat :call ClangFormat()

" Run ClangFormatIfModified automatically when saving a C or C++ file
"autocmd BufWritePre,FileWritePre *.h,*.hpp,*.c,*.cpp,*.cc,*.cu,*.cuh,*.hh :call ClangFormatIfModified()


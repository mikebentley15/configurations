setlocal foldmethod=indent
setlocal shiftwidth=4
setlocal tabstop=4

let s:git_root = trim(system("git rev-parse --show-toplevel"))
if !StartsWith(s:git_root, "fatal:")
  let s:project_name = trim(system("git remote -v | head -n1 | sed -e 's,^.*[:/],,g' -e 's, .*,,g' -e 's/.git$//g'"))
  if s:project_name is "nuDeep"
    execute 'setlocal path+=' . s:git_root . "/nuplan_internal/"
    execute 'setlocal path+=' . s:git_root . "/nuplan_devkit/"
  endif
endif

" undo these settings when exiting this plugin
if !exists("b:undo_ftplugin")
  let b:undo_ftplugin = "setl fdm< sw< ts<"
else
  let b:undo_ftplugin += "| setl fdm< sw< ts<"
endif

setlocal foldmethod=indent
setlocal shiftwidth=4
setlocal tabstop=4

" undo these settings when exiting this plugin
if !exists("b:undo_ftplugin")
  let b:undo_ftplugin = "setl fdm< sw< ts<"
else
  let b:undo_ftplugin += "| setl fdm< sw< ts<"
endif

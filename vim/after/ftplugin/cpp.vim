setlocal shiftwidth=4
setlocal tabstop=4

let s:git_root = trim(system("git rev-parse --show-toplevel"))
if !StartsWith(s:git_root, "fatal:")
  let s:project_name = trim(system("git remote -v | head -n1 | sed -e 's,^.*[:/],,g' -e 's, .*,,g' -e 's/.git$//g'"))
  " execute 'setlocal path+=' . s:git_root . "/"
  if s:project_name is "av-stack"
    setlocal path-=/usr/include
    execute 'setlocal path+=' . s:git_root . "/external/x86_64-linux-av-gcc-10.2.0/x86_64-av-linux-gnu/include/c++/10.2.0/**1"
    execute 'setlocal path+=' . s:git_root . "/external/x86_64-linux-av-gcc-10.2.0/x86_64-av-linux-gnu/sysroot/usr/include/"
    execute 'setlocal path+=' . s:git_root . "/external/x86_64-linux-av-gcc-10.2.0/lib/gcc/x86_64-av-linux-gnu/10.2.0/include/"
    execute 'setlocal path+=' . s:git_root . "/external/**2"
    execute 'setlocal path+=' . s:git_root . "/autonomy_tools/"
  endif
endif

" undo these settings when exiting this plugin
if !exists("b:undo_ftplugin")
  let b:undo_ftplugin = "setl sw< ts< path<"
else
  let b:undo_ftplugin += "| setl sw< ts< path<"
endif

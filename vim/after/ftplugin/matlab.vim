setlocal foldmethod=indent

" undo these settings when exiting this plugin
if !exists("b:undo_ftplugin")
  let b:undo_ftplugin = "setl fdm<"
else
  let b:undo_ftplugin += "| setl fdm<"
endif

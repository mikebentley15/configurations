setlocal shiftwidth=1
setlocal tabstop=1

" undo these settings when exiting this plugin
if !exists("b:undo_ftplugin")
  let b:undo_ftplugin = "setl sw< ts<"
else
  let b:undo_ftplugin += "| setl sw< ts<"
endif

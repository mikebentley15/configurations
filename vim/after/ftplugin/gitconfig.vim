setlocal noexpandtab

" undo these settings when exiting this plugin
if !exists("b:undo_ftplugin")
  let b:undo_ftplugin = "setl et<"
else
  let b:undo_ftplugin += "| setl et<"
endif

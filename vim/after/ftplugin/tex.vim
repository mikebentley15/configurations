" Undo some latex-suite macros
call IMAP('()', '()', 'tex')
call IMAP('{}', '{}', 'tex')
call IMAP('[]', '[]', 'tex')
call IMAP('::', '::', 'tex')
call IMAP('{{', '{{', 'tex')
call IMAP('((', '((', 'tex')
call IMAP('[[', '[[', 'tex')
call IMAP('$$', '$$', 'tex')
call IMAP('FEM', 'FEM', 'tex')

" We disable many features as found in the documentation
" http://vim-latex.sourceforge.net/documentation/latex-suite
let g:Tex_EnvironmentMaps=0
let g:Tex_EnvironmentMenus=0
let g:Tex_FontMaps=0
let g:Tex_FontMenus=0
let g:Tex_SectionMaps=0
let g:Tex_SectionMenus=0
let g:Tex_Diacritics=0 " do not replace diacritics
let g:Tex_SmartKeyDot=0 " auto insertion of \ldots
let g:Imap_UsePlaceHolders=0 " placeholders like <++>

"
" Smart key replacements
"
let g:Tex_SmartKeyQuote=1 " replace " with `` or ''
let g:Tex_SmartKeyBS=0 " erase full diacritics with one backspace

"
" Compiling and viewing
"
let g:Tex_UseMakefile=0 " don't use the Makefile if it's there
"let g:Tex_DefaultTargetFormat='pdf' " \ll compiles in this format
let g:Tex_CompileRule_pdf='pdflatex -shell-escape -interaction=nonstopmode $*'
let g:Tex_GoToError=1 " allow it to go to the file/line where compilation fails
"TTarget pdf " their command to fully change to this target format

"
" Window drop-down menus
"
let g:Tex_Menus=1 " whether to add window drop-down menus
let g:Tex_MainMenuLocation=900 " big number means put them at the end
let g:Tex_MathMenus=1 " include TeX-Math dropdown
let g:Tex_NestElementMenus=1 " use heirarchical structure in TeX-Elements dropdown
let g:Tex_PackagesMenu=1 " Generate TeX-Suite > Packages dropdowns
let g:Tex_NestPackagesMenu=1 " heirarchical structure in TeX-Suite > Packages
let g:Tex_MenuPrefix='Tex-' " prefix for name of all drop-down menus
let g:Tex_UseUtfMenus=1 " use utf-8 in drop-down menus

"
" Folding
"
let g:Tex_Folding=1 " enable folding
let g:Tex_AutoFolding=1 " enable folding

" specific commands and environments to fold.  Order matters, at least for
" sections.  I have copied the default values and added some of my own, even
" though adding to the default values is supported.  I just want these to be
" complete.
let g:Tex_FoldedSections='part,chapter,section,subsection,subsubsection,paragraph'
let g:Tex_FoldedEnvironments='verbatim,comment,eq,gather,align,figure,table,thebibliography,keywords,abstract,titlepage'
let g:Tex_FoldedCommands='caption,MB,AK,todo'
let g:Tex_FoldedMisc='item,preamble,<<<'

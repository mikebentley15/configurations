LINK         = ln -n -s -f
MOVE         = mv
MKDIR        = mkdir -p
MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST))) # if we don't want CURDIR

# for everything not specially used by the system
CONF_DIR    := $(HOME)/.config
MYCONF_DIR  := $(CONF_DIR)/bentley
BIN_DIR     := $(HOME)/bin

# for the backup target to move existing files here
# backup target moves existing FINAL_LINKS to here
BACKUP_DIR  := $(MYCONF_DIR)/backup

# all files/directories that will link to $(HOME)/.<name>
DOT  :=
DOT  += bash_completion
DOT  += bash_profile
DOT  += bashrc
DOT  += emacs
DOT  += npmrc
DOT  += screenrc
DOT  += tmux.conf
DOT  += vim
DOT  += vimrc
DOT  += xmodmap
DOT_LINKS := $(DOT:%=$(HOME)/.%)

# all files/directories that will link to $(CONF_DIR)/<name>
CONF := ranger
CONF_LINKS := $(CONF:%=$(CONF_DIR)/%)

# all files/directories that will link to $(MYCONF_DIR)/<name>
MYCONF  :=
MYCONF  += byobu
MYCONF  += emacs_scripts
MYCONF_LINKS := $(MYCONF:%=$(MYCONF_DIR)/%)

# directories that will link all contents to $(MYCONF_DIR)/<name>/*
# rather than linking the directory itself
MYCONF_SUB   :=
MYCONF_SUB   += bash_completion.d
MYCONF_SUB   += bashrc.d
MYCONF_SUB   += bash_profile.d
MYCONF_SUB_FILES := $(foreach f,$(MYCONF_SUB),$(wildcard $f/*))
MYCONF_SUB_LINKS := $(MYCONF_SUB_FILES:%=$(MYCONF_DIR)/%)

# all files/directories that will link to $(BIN_DIR)
BIN   :=
BIN   += bin/ampy
BIN   += bin/catmp4
BIN   += bin/copyftime
BIN   += bin/csv_uniq
BIN   += bin/csvcombine
BIN   += bin/csvtranspose
BIN   += bin/extract_frames
BIN   += bin/extstat
BIN   += bin/ffsilent
BIN   += bin/inotifyexec
BIN   += bin/json_pretty
BIN   += bin/mp4pad
BIN   += bin/mp4slim
BIN   += bin/mts2mp4
BIN   += bin/pandas_convert
BIN   += bin/pdfbw
BIN   += bin/pdfembed
BIN   += bin/pdfgrey
BIN   += bin/pdfshrink
BIN   += bin/piano-timer
BIN   += bin/pico-flash
BIN   += bin/record-microphone
BIN   += bin/strip_frames
BIN   += bin/svg2pdf
BIN   += bin/svg2pdftex
BIN   += bin/svg2png
BIN_LINKS := $(BIN:%=$(HOME)/%)

ALL_LINKS :=
ALL_LINKS += $(DOT_LINKS)
ALL_LINKS += $(CONF_LINKS)
ALL_LINKS += $(MYCONF_LINKS)
ALL_LINKS += $(MYCONF_SUB_LINKS)
ALL_LINKS += $(BIN_LINKS)

# TODO: add 3rdparty stuff?  Repos where I have forks?

# all symbolic links that are created through this makefile
FINAL_LINKS :=

PHONY_TARGETS :=
PHONY_TARGETS += default
PHONY_TARGETS += help
PHONY_TARGETS += all
PHONY_TARGETS += backup
PHONY_TARGETS += dot
PHONY_TARGETS += conf
PHONY_TARGETS += myconf
PHONY_TARGETS += bin
PHONY_TARGETS += extra_stuff

.PHONY: $(PHONY_TARGETS) extra_stuff

default: all

all:     $(ALL_LINKS) extra_stuff neovim
dot:     $(DOT_LINKS)
conf:    $(CONF_LINKS)
myconf:  $(MYCONF_LINKS) $(MYCONF_SUB_LINKS)
bin:     $(BIN_LINKS)

help:
	@echo "Targets: ${PHONY_TARGETS}"

extra_stuff:
	mkdir -p $(HOME)/.cache/vim/swap

backup:
	$(MKDIR) $(BACKUP_DIR)
	@for file in $(ALL_LINKS); do \
	  if [ -e "$$file" ]; then \
		  echo $$file -> $(BACKUP_DIR)/; \
	    $(MOVE) $$file $(BACKUP_DIR)/; \
	  fi \
	done

# rules to actually do the links
LINK_CMD  = $(MKDIR) $(dir $@) && $(LINK) $(abspath $<) $@

$(HOME)/.%: %
	-$(LINK_CMD)

$(CONF_DIR)/%: %
	-$(LINK_CMD)

$(MYCONF_DIR)/%: %
	-$(LINK_CMD)

$(HOME)/bin/%: bin/%
	-$(LINK_CMD)

.PHONY: neovim
neovim: $(CONF_DIR)/nvim nvim nvim-custom
	mkdir -p nvim/lua
	rm -rf nvim/lua/custom
	ln -s $(PWD)/nvim-custom nvim/lua/custom

$(CONF_DIR)/nvim/custom: nvim-custom
	mkdir -p $(CONF_DIR)
	ln -s $(PWD)/nvim-custom $@

#$(info ALL_LINKS=$(ALL_LINKS))


;;;;;  C++ Customizations

;; Allow for customizations
(require 'cc-mode)

;; Choose a style that doesn't indent the braces
;; Define my indentation styling
;(c-add-style "my-cpp-style"
;             '("stroustrup"
;               (indent-tabs-mode . nil)	 ; use spaces rather than tabs
;               (c-basic-offset . 2)			 ; indent using 2 spaces
;               (c-offsets-alist . ((inline-open . 0)		; custom indentation rules
;                                   (brace-list-open . 0)
;                                   (statement-case-open . 0)))))
;; Use my indentation styling
;(c-set-style "my-cpp-style")

(setq c-basic-offset . 4) ; indent using 4 spaces

;; Make it so that opening braces are not indented
(c-set-offset 'substatement-open 0)

;; Turn on syntax colorization
(global-font-lock-mode 1)

;; Set F5 to be a shortcut for compile
(global-set-key [(f5)] 'compile)

;; Make it so that the compile output window is not half
;; the screen
(setq compilation-window-height 8)

;; Make the compilation window go away if it compiles
;; without any errors
(setq compilation-finish-function
      (lambda (buf str)
	; (if (string-match "error" str)
	(if (string-match "exited abnormally" str)
	    ;; there were errors
	    (message "compilation errors, press C-x ` to visit")
	  ;; no errors, make the compilation window go away in
	  ;; 0.5 seconds
	  (run-at-time 0.5 nil 'delete-window-on buf)
	  (message "NO COMPILATION ERRORS!"))))

;; Enable hungry delete.  This will make it so that all 
;; whitespace around the cursor will be consumed when you
;; press Backspace or C-d.
(c-toggle-hungry-state 1)



;; Use only TAB for indentation
;; I don't understand how this code works
; (defun my-build-tab-stop-list (width)
					;  (let ((num-tab-stops (/ 80 width))
					;  (counter 1)
					;  (ls nil))
					;  (while (<= counter num-tab-stops)
					;  (setq 1s (cons (* width counter) 1s))
					;  (setq counter (1+ counter)))
					;  (set (make-local-variable 'tab-stop-list) (nreverse 1s))))
; (defun my-c-mode-common-hook ()
					;  (setq tab-width 2) ;; change this to taste, this is whate K&R uses
					;  (my-build-tab-stop-list tab-width)
					;  (setq c-basic-offset tab-width))
; (add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;; Even though I don't, the following commented out section is
;; if you want to use only spaces for indentation.
; (defun my-build-tab-stop-list (width)
;   (let ((num-tab-stops (/ 80 width))
; 	(counter 1)
; 	(ls nil))
;     (while (<= counter num-tab-stops)
;       (setq 1s (cons (* width counter) 1s))
;       (setq counter (1+ counter)))
;     (set (make-local-variable 'tab-stop-list) (nreverse 1s))))
; (defun my-c-mode-common-hook ()
;   (setq tab-width 2) ;; change this to taste, this is what K&R uses
;   (my-build-tab-stop-list tab-width)
;   (setq c-basic-offset tab-width)
;   (setq indent-tabs-mode nil));; force only spaces for indentation
; (add-hook 'c-mode-common-hook 'my-c-mode-common-hook)



(provide 'cpp-custom)
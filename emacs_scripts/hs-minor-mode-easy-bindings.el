;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;;;;;;;;;;;;;;;;;;;;;
;; hs-minor-mode-easy-bindings.el
;;
;; Installation: Store this file as hs-minor-mode-easy-bindings.el
;; somewhere in your load-path and create hooks for hideshow modes to
;; load this automatically, for example:

;; (add-hook 'hs-minor-mode-hook 'my-outline-easy-bindings)
;; (defun my-outline-easy-bindings ()
;;   (require 'outline-mode-easy-bindings nil t))



(let ((minor hs-minor-mode-map))
	(define-key minor (kbd "M-<left>") 'hs-hide-block)
	(define-key minor (kbd "M-<right>") 'hs-show-block)
	(define-key minor (kbd "C-M-<left>") 'hs-hide-level)
	)


(provide 'hs-minor-mode-easy-bindings)